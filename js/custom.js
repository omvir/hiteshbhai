var monthtext = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];

function populatedropdown(dayfield, monthfield, yearfield, isEdit){
    var today = new Date();
    var hidden_dayfield = "";
    var hidden_monthfield = "";
    var hidden_yearfield = "";
    
    if(isEdit){
        hidden_dayfield = dayfield;
        hidden_monthfield = monthfield;
        hidden_yearfield = yearfield;
    }
    
    var dayfield   = document.getElementById(dayfield);
    var monthfield = document.getElementById(monthfield);
    var yearfield  = document.getElementById(yearfield);
    
    for (var i = 1; i <= 31; i++){
        dayfield.options[i] = new Option(i, i);
        if(isEdit){
            dayfield.options[$('#hidden_' + hidden_dayfield).val()] = new Option($('#hidden_' + hidden_dayfield).val(), $('#hidden_' + hidden_dayfield).val(), true, true);
        } else {
            dayfield.options[today.getDate()] = new Option(today.getDate(), today.getDate(), true, true);
        }
    }
    
    for (var m = 0; m < 12; m++){
        monthfield.options[m] = new Option(monthtext[m], monthtext[m]);
        
        if(isEdit){
            var month_value = $('#hidden_' + hidden_monthfield).val() - 1;
            monthfield.options[month_value] = new Option(monthtext[month_value], monthtext[month_value], true, true);
        } else {
            monthfield.options[today.getMonth()] = new Option(monthtext[today.getMonth()], monthtext[today.getMonth()], true, true);
        }
    }
    
    var thisyear = today.getFullYear();
    var years = [];
    
    for (var y = thisyear - 1; y >= thisyear - 30; y--) { years.push(y); }
    
    years.sort();
    
    for (var y = thisyear; y < thisyear + 10; y++) { years.push(y); }
    
    for(var y = 0; y < years.length; y++){
        yearfield.options[years[y]] = new Option(years[y], years[y]);
        
        if(isEdit){
            yearfield.options[$('#hidden_' + hidden_yearfield).val()] = new Option($('#hidden_' + hidden_yearfield).val(), $('#hidden_' + hidden_yearfield).val(), true, true);
        } else {
            yearfield.options[thisyear] = new Option(thisyear, thisyear, true, true);
        }
    }
}