<?php
define("DEF", 1);
require_once 'config.php';
require_once CLASSPATH.'class.login.php';

$page = 'logout';
$session->loginCheck("admin_logged_in",$page);

$login = new Login();

$login->logout();