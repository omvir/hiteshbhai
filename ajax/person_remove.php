<?php
define("DEF", 1);
require_once '../config.php';
include_once CLASSPATH.'class.person.php';
include_once CLASSPATH.'class.persontoscheme.php';

$page = 'person';
$session->loginCheck("admin_logged_in",$page);

$person = new Person();

if($person->removePerson($_GET['person_id']))
    echo 'true';
else
    echo 'false';

$persontoscheme = new Persontoscheme();
$persontoscheme->removePSByPersonID($_GET['person_id']);
?>
