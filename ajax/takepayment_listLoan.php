<?php
define("DEF", 1);
require_once '../config.php';

include_once CLASSPATH.'class.takepayment.php';

$takepayment = new TakePayment();

$scheme_id = $_POST['scheme_id'];
$currentMonthYear = date('m-Y', strtotime($_POST['monthTakePaymentDate'] . $_POST['yearTakePaymentDate']));
$till_date = date('d M, Y', strtotime($_POST['dayTakePaymentDate'] . $_POST['monthTakePaymentDate'] . $_POST['yearTakePaymentDate']));
$post_date = date('Y-m-d', strtotime($_POST['dayTakePaymentDate'] . $_POST['monthTakePaymentDate'] . $_POST['yearTakePaymentDate']));

$paymentLoanList_res = $takepayment->getPersonLoanList($scheme_id);

$tableData = "";
while($paymentListList_data = mysql_fetch_assoc($paymentLoanList_res)){
    
    $pending_interest_array = "";
    $total_fine_days = "";
    
    $loan_id = $paymentListList_data['id'];
    mysql_query("DELETE FROM loan_interest WHERE loan_id = '" . $loan_id . "' AND is_paid = 'NO';");
    
    $date1 = new DateTime(date('Y-m-d', strtotime($_POST['yearTakePaymentDate'] . '-' . $_POST['monthTakePaymentDate'] . '-' . $_POST['dayTakePaymentDate'])));
    $date1->modify('last day of this month');
    $date2 = new DateTime(date('Y-m-d', strtotime($paymentListList_data['loan_date'])));
    $date2->modify('first day of this month');
    $interval = DateInterval::createFromDateString('1 month');
    $months = new DatePeriod($date2, $interval, $date1);
    
    foreach ($months as $dt) {
        
        $due_date = $dt->format("Y-m") . '-' . $paymentListList_data['interest_fine_start_day'];
        
        if(date('Y-m', strtotime($due_date)) != date('Y-m', strtotime($paymentListList_data['loan_date']))){
           
            // Calculate pending interest and pending fine with loan_interest table.
            $calc_int_fine_sql = mysql_query("SELECT * FROM loan_interest
                                              WHERE loan_id = " . $loan_id . "
                                              AND YEAR(due_date) = '" . date('Y',strtotime($due_date)) . "'
                                              AND MONTH(due_date) = '" . date('m',strtotime($due_date)) . "';");
            $calculation_data = mysql_fetch_assoc($calc_int_fine_sql);
            
            if(!isset($calculation_data['is_paid'])){
                
                $loan_pending_interest = ((float)$paymentListList_data['current_pending_amount'] * (float)$paymentListList_data['interest'] / 100);
                
                // Calculate Fine days between loan date and posted date.
                $fineDate1 = new DateTime(date('Y-m-d', strtotime($due_date)));
                $fineDate2 = new DateTime(date('Y-m-d', strtotime($post_date)));
                
                if(strtotime($due_date) <= strtotime($post_date)){
                    $total_fine_days[] = ((int)$fineDate1->diff($fineDate2)->days + 1);
                }
                
                // Validating current posted date's entry
                mysql_query("INSERT INTO loan_interest
                    SET loan_id = '" . $loan_id . "',
                        loan_pending_amount = '" . (float)$paymentListList_data['current_pending_amount'] . "',
                        interest_amount = '" . number_format($loan_pending_interest, 2) . "',
                        due_date = '" . $due_date . "',
                        is_paid = 'NO';");

                $pending_interest_array[] = $loan_pending_interest;
            }
        }
    }
    
    if(array_sum($pending_interest_array) > 0){
        $pending_interest = number_format(array_sum($pending_interest_array), 2);
    } else {
        $pending_interest = "0.00";
    }
    
    if(((int)array_sum($total_fine_days)) > 0){
        $pending_fine = number_format(((int)array_sum($total_fine_days) * (int)$paymentListList_data['interest_fine_amount']), 2);
    } else {
        $pending_fine = "0.00";
    }
    
    $total_pending_interest = (float)str_replace(',', '', $pending_interest);
    $total_pending_fine = (float)str_replace(',', '', $pending_fine);
    
    $tableData .= "<tr>";
    
        $tableData .= "<td>" . $paymentListList_data['person_name'] . "<input type='hidden' name='personLoanInfo[loan_id][]' value='" . $paymentListList_data['id'] . "' /></td>";
        $tableData .= "<td>" . date('d M, Y',strtotime($paymentListList_data['loan_date'])) . "</td>";
        $tableData .= "<td>" . $paymentListList_data['total_amount'] . ' [' . $paymentListList_data['interest'] . " %]</td>";
        $tableData .= "<td>" . $paymentListList_data['current_pending_amount'] . "</td>";
        
        if($currentMonthYear == date('m-Y',strtotime($paymentListList_data['loan_date']))){
            $tableData .= "<td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
        } else {
            $tableData .= "<td class='pending_interest' align='right'>" . number_format($total_pending_interest, 2, '.', '') . "</td>";
            $tableData .= "<td class='pending_fine' align='right'>" . number_format($total_pending_fine, 2, '.', '') . "</td>";
            $tableData .= "<td class='pending_total' align='right'>" . number_format(((float)$total_pending_interest + (float)$total_pending_fine), 2, '.', '') . "</td>";
            $tableData .= "<td><input class='intFineCheck' type='checkbox' name='personLoanInfo[check_box][]' onclick='intFineCheck(this);' /></td>";
            $tableData .= "<td><input class='input_pending_interest' type='text' name='personLoanInfo[interest][]' onblur='addTotal(this);' size='10' /></td>";
            $tableData .= "<td><input class='input_pending_fine' type='text' name='personLoanInfo[penalty][]' onblur='addTotal(this);' size='10' /></td>";
            $tableData .= "<td><input type='text' name='personLoanInfo[loan_amount][]' onblur='addTotal(this);' size='10' /></td>";
        }
        $tableData .= "<td align='right'><span class='column_total'></span></td>";

    $tableData .= "</tr>";
    
}
$tableData .= "<input type='hidden' id='post_date' name='post_date' value='" . $post_date . "' />";
$tableData .= "<input type='hidden' id='till_date' value='" . $till_date . "' />";

echo $tableData;exit;
?>