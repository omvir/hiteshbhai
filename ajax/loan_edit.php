<?php
define("DEF", 1);
require_once '../config.php';
include_once CLASSPATH.'class.person.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.loan.php';

$page = 'loan';
$person = new Person();
$scheme = new Scheme();
$loan = new Loan();

if(count($_POST) > 0)
{
    $loan_id = $loan->updateLoan($_POST);
}

$loan_id = $_REQUEST['loan_id'];

$person_res = $person->getPersons();
$scheme_res = $scheme->getSchemes();
$loan_res = $loan->getLoanInfoByID($loan_id);

?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php include '../msg.php'; ?>
            <form id="editLoanForm" action="" method="post" class="bv-form">
                <input type="hidden" id="loan_id" name="loan_id" value="<?php echo $loan_id;?>">
                <div class="box-body clearfix">
                    
                    <div class="form-group">
                        <label>Scheme</label>
                        <select class="form-control" id="scheme_id" name="scheme_id" required>
                            <option value="">Select Scheme</option>
                            <?php while($scheme_row = mysql_fetch_assoc($scheme_res)){ ?>
                            
                                <?php if($loan_res['scheme_id'] == $scheme_row['id']) { ?>

                                <option value="<?php echo $scheme_row['id']; ?>" selected=""><?php echo $scheme_row['scheme_name']; ?></option>
                                <?php } else { ?>

                                <option value="<?php echo $scheme_row['id']; ?>"><?php echo $scheme_row['scheme_name']; ?></option>

                                <?php } ?>
                                
                            <?php } ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Person</label>
                        <select class="form-control" id="person_id" name="person_id" required>
                            <option value="">Select Person</option>
                            <?php while($person_row = mysql_fetch_assoc($person_res)){ ?>

                                <?php if($loan_res['person_id'] == $person_row['id']) { ?>

                                <option value="<?php echo $person_row['id']; ?>" selected=""><?php echo $person_row['person_name']; ?></option>
                                <?php } else { ?>

                                <option value="<?php echo $person_row['id']; ?>"><?php echo $person_row['person_name']; ?></option>

                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Total Amount</label>
                        <input type="text" class="form-control" id="totalAmount" name="totalAmount" placeholder="Total Amount" required pattern="\d+(\.\d{2})?" value="<?php echo $loan_res['total_amount']; ?>" />
                    </div>
                    
                    <div class="form-group">
                        <label>Interest %</label>
                        <input type="text" class="form-control" id="interest" name="interest" value="<?php echo $loan_res['interest']; ?>" placeholder="%" required />
                    </div>
                    
                    <div class="form-group">
                        <label>Loan Date</label>
                        <div class="clearfix">
                            <div class="col-md-4">
                                <input type="hidden" id="hidden_popupDayLoanDate" name="hidden_popupDayLoanDate" value="<?php echo date('j', strtotime($loan_res['loan_date'])); ?>" />
                                <select id="popupDayLoanDate" name="popupDayLoanDate" class="form-control"></select>
                            </div>
                            <div class="col-md-4">
                                <input type="hidden" id="hidden_popupMonthLoanDate" name="hidden_popupMonthLoanDate" value="<?php echo date('m', strtotime($loan_res['loan_date'])); ?>" />
                                <select id="popupMonthLoanDate" name="popupMonthLoanDate" class="form-control"></select>
                            </div>
                            <div class="col-md-4">
                                <input type="hidden" id="hidden_popupYearLoanDate" name="hidden_popupYearLoanDate" value="<?php echo date('Y', strtotime($loan_res['loan_date'])); ?>" />
                                <select id="popupYearLoanDate" name="popupYearLoanDate" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="box-footer">
                    <button type="submit" name="updateLoan" class="btn btn-danger" value="update">Update Loan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#editLoanForm').bootstrapValidator().on('success.form.bv', function(e) {
    e.preventDefault();
    var $form = $(e.target);
    var bv = $form.data('bootstrapValidator');
    $.post('./ajax/loan_edit.php', $form.serialize(), function(result) {
        $('#loanModal .te').html(result);
    });
});

populatedropdown("popupDayLoanDate", "popupMonthLoanDate", "popupYearLoanDate", true);

</script>