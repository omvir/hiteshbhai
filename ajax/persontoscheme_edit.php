<?php
define("DEF", 1);
require_once '../config.php';
include_once CLASSPATH.'class.person.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.persontoscheme.php';

$page = 'persontoscheme';
$person = new Person();
$scheme = new Scheme();
$personToScheme = new PersonToScheme();

if(count($_POST) > 0)
{
    $ps_id = $personToScheme->updatePersonScheme($_POST);
}

$ps_id = $_REQUEST['ps_id'];

$person_res = $person->getPersons();
$scheme_res = $scheme->getSchemes();
$psInfo = $personToScheme->getPSInfoByID($ps_id);

?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php include '../msg.php'; ?>
            <form id="editPersonForm" action="" method="post" class="bv-form">
                <input type="hidden" id="ps_id" name="ps_id" value="<?php echo $ps_id;?>">
                <div class="box-body clearfix">
                    <div class="form-group">
                        <label>Person</label>
                        <select class="form-control" id="person_id" name="person_id" required>
                            <option value="">Select Person</option>
                            <?php while($person_row = mysql_fetch_assoc($person_res)){ ?>

                                <?php if($psInfo['person_id'] == $person_row['id']) { ?>

                                <option value="<?php echo $person_row['id']; ?>" selected=""><?php echo $person_row['person_name']; ?></option>
                                <?php } else { ?>

                                <option value="<?php echo $person_row['id']; ?>"><?php echo $person_row['person_name']; ?></option>

                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Scheme</label>
                        <select class="form-control" id="scheme_id" name="scheme_id" required>
                            <option value="">Select Scheme</option>
                            <?php while($scheme_row = mysql_fetch_assoc($scheme_res)){ ?>
                            
                                <?php if($psInfo['scheme_id'] == $scheme_row['id']) { ?>

                                <option value="<?php echo $scheme_row['id']; ?>" selected=""><?php echo $scheme_row['scheme_name']; ?></option>
                                <?php } else { ?>

                                <option value="<?php echo $scheme_row['id']; ?>"><?php echo $scheme_row['scheme_name']; ?></option>

                                <?php } ?>
                                
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Loan Amount</label>
                        <input type="text" class="form-control" id="loanAmount" name="loanAmount" placeholder="Loan Amount" required pattern="\d+(\.\d{2})?" value="<?php echo $psInfo['loan_amount']; ?>" />
                    </div>
                    
                    <div class="form-group">
                        <label>Loan Date</label>
                        <div class="clearfix">
                            <div class="col-md-4">
                                <input type="hidden" id="hidden_popupDayLoanDate" name="hidden_popupDayLoanDate" value="<?php echo date('d', strtotime($psInfo['loan_issue_date'])); ?>" />
                                <select id="popupDayLoanDate" name="popupDayLoanDate" class="form-control"></select>
                            </div>
                            <div class="col-md-4">
                                <input type="hidden" id="hidden_popupMonthLoanDate" name="hidden_popupMonthLoanDate" value="<?php echo date('m', strtotime($psInfo['loan_issue_date'])); ?>" />
                                <select id="popupMonthLoanDate" name="popupMonthLoanDate" class="form-control"></select>
                            </div>
                            <div class="col-md-4">
                                <input type="hidden" id="hidden_popupYearLoanDate" name="hidden_popupYearLoanDate" value="<?php echo date('Y', strtotime($psInfo['loan_issue_date'])); ?>" />
                                <select id="popupYearLoanDate" name="popupYearLoanDate" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Interest %</label>
                        <input type="text" class="form-control" id="interest" name="interest" value="<?php echo $psInfo['loan_interest_percent']; ?>" placeholder="%" required />
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" name="updatePerson" class="btn btn-danger" value="update">Update Person's Scheme</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#editPersonForm').bootstrapValidator().on('success.form.bv', function(e) {
    e.preventDefault();
    var $form = $(e.target);
    var bv = $form.data('bootstrapValidator');
    $.post('./ajax/persontoscheme_edit.php', $form.serialize(), function(result) {
        $('#psModal .te').html(result);
    });
});

populatedropdown("popupDayLoanDate", "popupMonthLoanDate", "popupYearLoanDate", true);

</script>