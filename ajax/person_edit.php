<?php
define("DEF", 1);
require_once '../config.php';
include_once CLASSPATH.'class.person.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.persontoscheme.php';

$page = 'person';
$session->loginCheck("admin_logged_in",$page);

$person = new Person();

$persontoscheme = new Persontoscheme();

if(count($_POST) > 0)
{
	$persontoscheme->removePSByPersonID($_POST['person_id']);
    $scheme_ids = $_POST['scheme_ids'];
    $person_id = $person->updatePerson($_POST);
    foreach ($scheme_ids as $scheme_id) {
        $data['person_id'] = $person_id;
        $data['scheme_id'] = $scheme_id;
        $data['loanAmount'] = 0.00;
        $data['interest'] = 0.00;
        $data['dayLoanDate'] = 00;
        $data['monthLoanDate'] = 00;
        $data['yearLoanDate'] = 0000;
        $persontoschemeid = $persontoscheme->addPersonScheme($data);
    }
}

$person_id = $_REQUEST['person_id'];

$person_data = $person->getPersonInfo($person_id);
$person_info = mysql_fetch_assoc($person_data);

$scheme = new Scheme();
$scheme_res = $scheme->getSchemes();

$scheme_data = $persontoscheme->getPSInfoByPersonID($person_id);
$scheme_array = array();
$key = 0;
while($personscheme_row = mysql_fetch_assoc($scheme_data)){
	$scheme_array[$key]['scheme_id'] = $personscheme_row['scheme_id'];
	$key++;
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php include '../msg.php'; ?>
            <form id="editPersonForm" action="" method="post" class="bv-form">
                <input type="hidden" id="person_id" name="person_id" value="<?php echo $person_id;?>">
                <div class="box-body clearfix">
                    <div class="form-group">
                        <label>Person Name</label>
                        <input type="text" class="form-control" id="person_name" name="person_name" placeholder="Person Name" value="<?php echo $person_info['person_name']; ?>" data-bv-notempty="true" data-bv-notempty-message="The person name is required and cannot be empty">
                    </div>
                    <div class="form-group">
                        <label>Scheme Name</label>
                        <select class="form-control" name="scheme_ids[]" multiple="multiple">
                            <?php while($scheme_row = mysql_fetch_assoc($scheme_res)){
                            		$selected = "";
									foreach ($scheme_array as $key => $value) {
										if($value['scheme_id'] == $scheme_row['id'])
										{
											$selected = "selected='selcted'";
										}
									}
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo $scheme_row['id']; ?>"><?php echo $scheme_row['scheme_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Phone 1</label>
                        <input type="text" class="form-control" id="phone1" name="phone1" placeholder="Phone No 1" value="<?php echo $person_info['phone1']; ?>">
                    </div>
                    <div class="form-group">
                        <label>Phone 2</label>
                        <input type="text" class="form-control" id="phone2" name="phone2" placeholder="Phone No 2" value="<?php echo $person_info['phone2']; ?>">
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" name="updatePerson" class="btn btn-danger" value="update">Update Person</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#editPersonForm').bootstrapValidator().on('success.form.bv', function(e) {
    e.preventDefault();
    var $form = $(e.target);
    var bv = $form.data('bootstrapValidator');
    $.post('./ajax/person_edit.php', $form.serialize(), function(result) {
        $('#personModal .te').html(result);
    });
});

</script>