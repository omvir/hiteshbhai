<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH . 'class.login.php';

$page = 'login';
$session->loginCheck("admin_logged_in",$page);

$login = new Login();
if (isset($_POST['submit'])) {
    $r = $login->verifyAdminLogin($_POST);
}

include_once 'includes/header.php';
?>
</head>
<body class="bg-black">

    <div class="form-box" id="login-box">
        <div class="header">Sign In</div>
        
        <form action="#" method="post">
            <div class="body bg-gray">
                <?php
                $msg = $session->getFlash("msg");
                $type = $msg['type'];
                $text = $msg['text'];
                if(isset($r) && $r == FALSE)
                {
                ?>
                <div class="alert-danger">
                    <b>Alert!</b> Incorrect Username or Password.
                </div>
                <?php
                }
                ?>
                <div class="form-group">
                    <input type="text" name="uname" class="form-control" autofocus placeholder="Username"/>
                </div>
                <div class="form-group">
                    <input type="password" name="pass" class="form-control" placeholder="Password"/>
                </div>          
            </div>
            <div class="footer">                                                               
                <button type="submit" name="submit" class="btn bg-olive btn-block">Sign me in</button>  
            </div>
        </form>
    </div>
</body>
</html>