<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.loan.php';
include_once CLASSPATH.'class.loancredit.php';

$page = 'loan';
$todayDate = date('d-m-Y');

$loan_credited = new LoanCredit();

$credited_res = $loan_credited->getCreditedDetail();

include_once 'includes/header.php';
?>
<link href="<?php echo BASEURL; ?>css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

<link href="<?php echo BASEURL; ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo BASEURL; ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<link href="<?php echo BASEURL; ?>css/bootstrap-validator/bootstrap.validator.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/bootstrap-validator/bootstrap.validator.min.js" type="text/javascript"></script>

<script src="<?php echo BASEURL; ?>js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    
window.onload=function(){
    populatedropdown("dayLoanDate", "monthLoanDate", "yearLoanDate", false);
}

$(document).ready(function() {
    $('#tbl_loanList').dataTable({
        "iDisplayLength":500,
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        /* set up row grouping */
            "fnDrawCallback": function (oSettings) {
            if (oSettings.aiDisplay.length == 0) {
                return;
            }

            var nTrs = $('.table tbody tr');
            var iColspan = nTrs[0].getElementsByTagName('td').length;
            var sLastGroup = "";
            for (var i = 0; i < nTrs.length; i++) {
                $(".footer-total-amount-total").html( parseInt($(".footer-total-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-total-amount').html()));
                $(".footer-interest-amount-total").html( parseInt($(".footer-interest-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-interest-amount').html()));
                $(".footer-penalty-amount-total").html( parseInt($(".footer-penalty-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-penalty-amount').html()));
                $(".footer-payment-amount-total").html( parseInt($(".footer-payment-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-payment-amount').html()));
                $(".footer-current-amount-total").html( parseInt($(".footer-current-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-current-amount').html()));
            }
        },
    });
    
    $('[data-toggle="modal"]').click(function(e) {
        e.preventDefault();
        var ps_id = $(this).attr('id').replace('loanId_', '');
        
        var url = './ajax/loan_edit.php?loan_id=' + ps_id;
        
        if (url.indexOf('#') == 0) {
            $('#loanModal').modal('show');}
        else {
            $.get(url, function (data) {
                $('#loanModal .te').html(data);
                $('#loanModal').modal();
            }).success(function () {
                $('input:text:visible:first').focus();
            });
        }
    });
    
    $('.launchConfirm').on('click', function (e) {
        e.preventDefault();
        $obj = $(this);
        $('#confirm').modal().one('click', '#delete', function (e) {
            var ps_id = $obj.attr('id').replace('loanId_', '');
            var url = './ajax/loan_remove.php?&loan_id=' + ps_id;
            $.get(url, function (data) {
                if(data == 'true')
                    window.location.href = window.location.href;
                else{
                    $('#confirm').modal('show');
                    $('#confirm .te').html("<p class='text-red'>Loan not deleted! Please try again.</p>");
                }
            });
        });
    });
    
    getSchemePersons($('#scheme_id'));
    
    $('#scheme_id').change(function(){
        getSchemePersons($(this));
    });
});

function getSchemePersons(obj){
    $.ajax({
        type: 'post',
        url: './ajax/get_persons_by_scheme.php',
        data: { scheme_id : $(obj).val()},
        beforeSend: function(){
            $('#person_id').attr({'disabled' : 'disabled'});
        },
        success: function(data){
            $('#person_id').removeAttr('disabled');
            $('#person_id').html(data);
        }
    });
}
</script>
<style>
	.table > tbody > tr > td{padding: 5px;}
</style>
<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>
            
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Loan Credited Details</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <table id="tbl_loanList" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Loan Credit</th>
                                                <th>Credit Date</th>
											</tr>
                                        </thead>
                                        <tbody>
                                            <?php while($loan_data = mysql_fetch_assoc($credited_res)) {  ?>
                                            
                                            <tr>
                                                
												<td class="footer-interest-amount" align="right"><?php if($loan_data['loan_credit'] != ""){ echo $loan_data['loan_credit']; }else{ echo "0"; } ?></td>
												<td class="footer-penalty-amount" align="right"><?php if($loan_data['loan_credit_date'] != ""){ echo $loan_data['loan_credit_date']; }else{ echo "0"; } ?></td>
                                                
                                                
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <th class="footer-interest-amount-total" style="text-align: right;">0</th>
                                            <th colspan="2" align="center">Total</th>
                                            
                                           
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
        </div><!-- ./wrapper -->
        
        <!-- /.modal -->
        <div class="modal fade" id="loanModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Loan</h4>
                    </div>
                    <div class="modal-body"><div class="te">Please wait...</div></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.href = window.location.href;">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        <div id="confirm" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        Are you sure?
                        <div class="te"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
                        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
