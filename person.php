<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH.'class.person.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.persontoscheme.php';

$page = 'person';
$session->loginCheck("admin_logged_in",$page);

$person = new Person();

if(isset($_POST['submit']))
{
    $persontoscheme = new Persontoscheme();
    $scheme_ids = $_POST['scheme_ids'];
    $person_id = $person->addPerson($_POST);
    foreach ($scheme_ids as $scheme_id) {
        $data['person_id'] = $person_id;
        $data['scheme_id'] = $scheme_id;
        $data['loanAmount'] = 0.00;
        $data['interest'] = 0.00;
        $data['dayLoanDate'] = 00;
        $data['monthLoanDate'] = 00;
        $data['yearLoanDate'] = 0000;
        $persontoschemeid = $persontoscheme->addPersonScheme($data);
    }
}

$person_res = $person->getPersons();

$scheme = new Scheme();
$scheme_res = $scheme->getSchemes();

include_once 'includes/header.php';
?>

<link href="<?php echo BASEURL; ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo BASEURL; ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<link href="<?php echo BASEURL; ?>css/bootstrap-validator/bootstrap.validator.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/bootstrap-validator/bootstrap.validator.min.js" type="text/javascript"></script>

<script type="text/javascript">
var validator;
$(document).ready(function() {
    
    $('#tbl_personlist').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": false,
        "bInfo": true,
        "bAutoWidth": false
    });
    
    $('[data-toggle="modal"]').click(function(e) {
        e.preventDefault();
        var person_id = $(this).attr('id').replace('personId_', '');
        
        var url = './ajax/person_edit.php?person_id=' + person_id;
        
        if (url.indexOf('#') == 0) {
            $('#personModal').modal('show');}
        else {
            $.get(url, function (data) {
                $('#personModal .te').html(data);
                $('#personModal').modal();
            }).success(function () {
                $('input:text:visible:first').focus();
            });
        }
    });
    
    $('.launchConfirm').on('click', function (e) {
        e.preventDefault();
        $obj = $(this);
        $('#confirm').modal().one('click', '#delete', function (e) {
            var person_id = $obj.attr('id').replace('personId_', '');
            var url = './ajax/person_remove.php?&person_id=' + person_id;
            $.get(url, function (data) {
                if(data == 'true')
                    window.location.href = window.location.href;
                else{
                    $('#confirm').modal('show');
                    $('#confirm .te').html('<p class="text-red">Person not deleted! Please try again.</p>');
                }
            });
        });
    });
});
</script>

<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Add Person</h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Home </li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <form id="personForm" action="" method="post">
                                    <div class="box-body clearfix">
                                        <div class="form-group col-md-4">
                                            <label>Person Name</label>
                                            <input type="text" class="form-control" id="person_name" name="person_name" placeholder="Person Name" required>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Scheme Name</label>
                                            <select class="form-control" name="scheme_ids[]" multiple="multiple">
                                            <?php while($scheme_row = mysql_fetch_assoc($scheme_res)){
                                            	$select = "";
												if($scheme_row['id'] == DEFAULT_SCHEME_ID)
												{
													$select = "selected='selected'";
												}
                                            ?>
                                                <option <?php echo $select; ?> value="<?php echo $scheme_row['id']; ?>"><?php echo $scheme_row['scheme_name']; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Phone 1</label>
                                            <input type="text" class="form-control" id="phone1" name="phone1" placeholder="Phone No 1">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Phone 2</label>
                                            <input type="text" class="form-control" id="phone2" name="phone2" placeholder="Phone No 2">
                                        </div>
                                        
                                        <div class="form-group col-md-2">
                                            <label>&nbsp;</label>
                                            <button id="submit_person" type="submit" name="submit" class="form-control btn btn-primary" value="add">Add Person</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Person List</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <div class="box-body clearfix">
                                    <table id="tbl_personlist" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Person Name</th>
                                                <th>Phone 1</th>
                                                <th>Phone 2</th>
                                                <th>Created Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            while($person_row = mysql_fetch_assoc($person_res))
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $person_row['person_name']; ?></td>
                                                <td><?php echo $person_row['phone1']; ?></td>
                                                <td><?php echo $person_row['phone2']; ?></td>
                                                <td><?php echo date('d F, Y',strtotime($person_row['created_date'])); ?></td>
                                                <td>
                                                   <a id="personId_<?php echo $person_row['id']; ?>" class="input-mini btn btn-warning" data-toggle="modal">
                                                       <i class="glyphicon glyphicon-edit"></i>
                                                   </a>
                                                   <a id="personId_<?php echo $person_row['id']; ?>" class="launchConfirm input-mini btn btn-danger">
                                                       <i class="glyphicon glyphicon-remove"></i>
                                                   </a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
        </div><!-- ./wrapper -->
        
        <!-- /.modal -->
        <div class="modal fade" id="personModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Person</h4>
                    </div>
                    <div class="modal-body"><div class="te">Please wait...</div></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.href = window.location.href;">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        
        <div id="confirm" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        Are you sure?
                        <div class="te"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
                        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
