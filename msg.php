<?php
defined("DEF") or die("Restricted Direct Entry...");

$msg = $session->getFlash("msg");
if(!empty($msg))
{
    $type = $msg['type'];
    $text = $msg['text'];

    if ($type == "success") {
        ?>
<!--        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Success!</b> <?php echo $text; ?>
        </div>-->
        <div class="callout callout-info">
            <h4>Success!</h4>
            <p><?php echo $text; ?></p>
        </div>
        <?php
    } else if($type == "error") {
        ?>
<!--        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> <?php echo $text; ?>
        </div>-->
        <div class="callout callout-danger">
            <h4>Alert!</h4>
            <p><?php echo $text; ?></p>
        </div>
        <?php
    }
}