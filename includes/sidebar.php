<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="img/avatar3.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, <?php echo $_SESSION['admin_logged_in']['u_name']; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="<?php echo ($page == 'dashboard')?"active":""; ?>">
                <a href="dashboard.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            
            <li class="treeview <?php echo ($page == 'person')?"active":""; ?>">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Person Manager</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="person.php"><i class="fa fa-angle-double-right"></i>Add Person</a></li>
                </ul>
            </li>
            
            <li class="treeview <?php echo ($page == 'scheme')?"active":""; ?>">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Scheme Manager</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="scheme.php"><i class="fa fa-angle-double-right"></i>Add Scheme</a></li>
                    <li><a href="schemelist.php"><i class="fa fa-angle-double-right"></i>Scheme List</a></li>
                </ul>
            </li>
            
            <li class="<?php echo ($page == 'takepayment')?"active":""; ?>">
                <a href="takepayment.php">
                    <i class="fa fa-rupee"></i> <span>Scheme Payment</span>
                </a>
            </li>
            
            <li class="treeview <?php echo ($page == 'loan')?"active":""; ?>">
                <a href="#">
                    <i class="fa fa-briefcase"></i> <span>Loan Manager</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="loanadd.php"><i class="fa fa-angle-double-right"></i>Loan Add</a></li>
                    <li><a href="takepaymentLoan.php"><i class="fa fa-angle-double-right"></i>Loan Payment</a></li>
                    </ul>
            </li>
            
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>