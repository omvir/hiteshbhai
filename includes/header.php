<?php
$set_html_class = '';
if (isset($page) && $page == 'login')
    $set_html_class = "class='bg-black'";
?>
<!DOCTYPE html>
<html <?php echo $set_html_class; ?>>
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo BASEURL; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!--<link href="<?php echo BASEURL; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />-->
        <link href="<?php echo BASEURL; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo BASEURL; ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo BASEURL; ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


        <script src="<?php echo BASEURL; ?>js/jquery.min.js"></script>
        <script src="<?php echo BASEURL; ?>js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo BASEURL; ?>js/AdminLTE/app.js" type="text/javascript"></script>
