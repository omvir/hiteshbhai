<?php

class TakePayment
{
    private $session;
    
    function __construct()
    {
        $this->session = new SessionSet();
    }
    
    function addPersonLoanInterestAndFine($data){
    	
        $post_date = $data['post_date_content'];
        $temp = array();
        for($i = 0; $i < count($data['personLoanInfo']['loan_id']); $i++){
            
            $loan_id = $data['personLoanInfo']['loan_id'][$i];
            $loan_amount = ($data['personLoanInfo']['loan_amount'][$i] > 0) ? number_format($data['personLoanInfo']['loan_amount'][$i], 2, '.', '') : 0;
            
            if((isset($data['personLoanInfo']['interest'][$i]) && $data['personLoanInfo']['interest'][$i] != "") && (isset($data['personLoanInfo']['penalty'][$i]) && $data['personLoanInfo']['penalty'][$i] != "")){
        
            $total_fine_days = "";

            $interest = ($data['personLoanInfo']['interest'][$i] > 0) ? number_format($data['personLoanInfo']['interest'][$i], 2, '.', '') : 0;
            $penalty = ($data['personLoanInfo']['penalty'][$i] > 0) ? number_format($data['personLoanInfo']['penalty'][$i], 2, '.', '') : 0;
            
            $scheme_data_res = $this->getSchemeInfoByLoanId($loan_id);
            $scheme_data = mysql_fetch_assoc($scheme_data_res);
            
            mysql_query("DELETE FROM loan_interest WHERE loan_id = '" . $loan_id . "' AND is_paid = 'NO';");

            $date1 = new DateTime(date('Y-m-d', strtotime($data['post_date_content'])));
            $date1->modify('last day of this month');
            $date2 = new DateTime(date('Y-m-d', strtotime($scheme_data['loan_date'])));
            $date2->modify('first day of this month');
            $interval = DateInterval::createFromDateString('1 month');
            $months = new DatePeriod($date2, $interval, $date1);
            
            foreach ($months as $dt) {

                $due_date = $dt->format("Y-m") . '-' . $scheme_data['interest_fine_start_day'];

                if(date('Y-m', strtotime($due_date)) != date('Y-m', strtotime($scheme_data['loan_date']))){

                    // Calculate pending interest and pending fine with loan_interest table.
                    $calc_int_fine_sql = mysql_query("SELECT * FROM loan_interest
                                                      WHERE loan_id = " . $loan_id . "
                                                      AND YEAR(due_date) = '" . date('Y',strtotime($due_date)) . "'
                                                      AND MONTH(due_date) = '" . date('m',strtotime($due_date)) . "';");
                    $calculation_data = mysql_fetch_assoc($calc_int_fine_sql);

                    if(!isset($calculation_data['is_paid'])){
                        
                        $loan_pending_interest = number_format((float)$scheme_data['current_pending_amount'] * (float)$scheme_data['interest'] / 100, 2);
                        
                        // Calculate Fine days between loan date and posted date.
                        $fineDate1 = new DateTime(date('Y-m-d', strtotime($due_date)));
                        $fineDate2 = new DateTime(date('Y-m-d', strtotime($post_date)));

                        if(strtotime($due_date) <= strtotime($post_date)){
                            $total_fine_days = ((float)$fineDate1->diff($fineDate2)->days + 1);
                            $total_fine = (float)((float)$total_fine_days * (float)$scheme_data['interest_fine_amount']);
                        }
                        
                        // Validating current posted date's entry
                        mysql_query("INSERT INTO loan_interest
                            SET loan_id = '" . $loan_id . "',
                                loan_pending_amount = '" . (float)$scheme_data['current_pending_amount'] . "',
                                interest_amount = '" . number_format($loan_pending_interest, 2, '.', '') . "',
                                due_date = '" . $due_date . "',
                                is_paid = 'YES',
                                fine_amount_received = " . mysql_real_escape_string(number_format($total_fine, 2, '.', '')) . ",
                                receive_date = now();");
                    }
                }
            }
            
            // Credit Interest and Fine to Current Pending Amount in Loan Table
            //$currentPendingAmount = (float)$interest + (float)$penalty;
            //mysql_query("UPDATE loan SET current_pending_amount = current_pending_amount + " . $currentPendingAmount . " WHERE id = " . $loan_id . ";");

            // Debit loan_amount from Current Pending Amount in Loan Table
            mysql_query("UPDATE loan SET current_pending_amount = current_pending_amount - " . $loan_amount . " WHERE id = " . $loan_id . ";");
            
            // Add Amount in to loan_credit table
			$sql = "INSERT INTO loan_credit (loan_id, loan_credit, loan_credit_date, created_date) VALUES (".$loan_id.",".$loan_amount.", '".$post_date."', '".date('Y-m-d H:i:s',time())."')";
	        mysql_query($sql);
			//array_push($temp,$sql);
            } else {
                if(((float)$loan_amount) > 0){
                	
                    // Debit loan_amount from Current Pending Amount in Loan Table
                    mysql_query("UPDATE loan SET current_pending_amount = current_pending_amount - " . $loan_amount . " WHERE id = " . $loan_id . ";");
					
					// Add Amount in to loan_credit table
					$sql = "INSERT INTO loan_credit (loan_id, loan_credit, loan_credit_date, created_date) VALUES (".$loan_id.",".$loan_amount.", '".$post_date."', '".date('Y-m-d H:i:s',time())."')";
			        mysql_query($sql);
					//array_push($temp,$sql);
                }
            }
        }
        $temp['type'] = 'success';
        $temp['text'] = "Payment Taken Successfully";
        $this->session->setFlash("msg", $temp);
    }
    
    function getSchemeInfoByLoanId($loan_id){
        $sql = "SELECT l.*, p.person_name, s.scheme_name, s.interest_fine_start_day, s.interest_fine_amount
                FROM loan l
                LEFT JOIN person p ON l.person_id = p.id
                LEFT JOIN scheme s ON l.scheme_id = s.id
                WHERE l.id = " . $loan_id . "";
        
        $res = mysql_query($sql);
        return $res;
    }
}