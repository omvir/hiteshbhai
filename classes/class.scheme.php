<?php

class Scheme
{
    private $session;
    function __construct()
    {
        $this->session = new SessionSet();
    }
    
    function addScheme($data)
    {
        $scheme = mysql_real_escape_string($data['scheme']);
        $inst_amount = mysql_real_escape_string($data['inst_amount']);
        $inst_fineday = mysql_real_escape_string($data['inst_fineday']);
        $inst_fine_amount = mysql_real_escape_string($data['inst_fine_amount']);
        $interest_fineday = mysql_real_escape_string($data['interest_fineday']);
        $interest_fine_amount = mysql_real_escape_string($data['interest_fine_amount']);
        
        $sql = "INSERT INTO scheme SET "
                ."scheme_name = '".$scheme."',"
                ."installment_amount = '".$inst_amount."',"
                ."fine_start_day = '".$inst_fineday."',"
                ."installment_fine_amount = '".$inst_fine_amount."',"
                ."interest_fine_start_day = '".$interest_fineday."',"
                ."interest_fine_amount = '".$interest_fine_amount."',"
                ."created_date = now(),"
                ."updated_date = now()";
        mysql_query($sql);
        
        $id = mysql_insert_id();
        if($id > 0)
        {
            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = 'Scheme Added Successfully';
            $this->session->setFlash("msg", $temp);
            return $id;
        }
        else
        {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = 'Scheme Not Added';
            $this->session->setFlash("msg", $temp);
            return false;
        }
    }
    
    function getSchemes()
    {
        $sql = "SELECT * FROM scheme ORDER BY created_date DESC";
        $res = mysql_query($sql);
        return $res;
    }
    
    function get_person_loan_schemes($person_id){
        $sql = "SELECT l.scheme_id, s.scheme_name FROM loan l
                LEFT JOIN scheme s ON l.scheme_id = s.id
                WHERE l.person_id = " . $person_id . "
                ORDER BY s.id";
        $res = mysql_query($sql);
        return $res;
    }
}