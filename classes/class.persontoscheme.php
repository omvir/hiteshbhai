<?php

class PersonToScheme
{
    private $session;
    
    function __construct()
    {
        $this->session = new SessionSet();
    }
    
    function addPersonScheme($data){
        $person_id = mysql_real_escape_string($data['person_id']);
        $scheme_id = mysql_real_escape_string($data['scheme_id']);
        //$loanAmount = mysql_real_escape_string($data['loanAmount']);
        //$interest = mysql_real_escape_string($data['interest']);
        
        //$dayLoanDate = mysql_real_escape_string($data['dayLoanDate']);
        //$monthLoanDate = mysql_real_escape_string($data['monthLoanDate']);
        //$yearLoanDate = mysql_real_escape_string($data['yearLoanDate']);
        //$loanDate = date('Y-m-d', strtotime($yearLoanDate . '-' . date('m', strtotime($monthLoanDate)) . '-' .  $dayLoanDate));
        
        $sql = "INSERT INTO person_scheme SET "
                ."person_id = '".$person_id."',"
                ."scheme_id = '".$scheme_id."',"
                //."loan_amount = '".$loanAmount."',"
                //."loan_issue_date = '".$loanDate."',"
                //."loan_interest_percent = '".$interest."',"
                ."created_date = now(),"
                ."updated_date = now()";
        mysql_query($sql);
        
        $id = mysql_insert_id();
        if($id > 0)
        {
            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = "Person's Scheme Added Successfully";
            $this->session->setFlash("msg", $temp);
            return $id;
        }
        else
        {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = "Person's Scheme Not Added";
            $this->session->setFlash("msg", $temp);
            return false;
        }
    }
    
    function updatePersonScheme($data){
        $ps_id = mysql_real_escape_string($data['ps_id']);
        $person_id = mysql_real_escape_string($data['person_id']);
        $scheme_id = mysql_real_escape_string($data['scheme_id']);
        //$loanAmount = mysql_real_escape_string($data['loanAmount']);
        //$interest = mysql_real_escape_string($data['interest']);
        
        //$dayLoanDate = mysql_real_escape_string($data['popupDayLoanDate']);
        //$monthLoanDate = mysql_real_escape_string($data['popupMonthLoanDate']);
        //$yearLoanDate = mysql_real_escape_string($data['popupYearLoanDate']);
        //$loanDate = date('Y-m-d', strtotime($yearLoanDate . '-' . date('m', strtotime($monthLoanDate)) . '-' .  $dayLoanDate));
        
        $sql = "UPDATE person_scheme SET "
                ."person_id = '".$person_id."',"
                ."scheme_id = '".$scheme_id."',"
                //."loan_amount = '".$loanAmount."',"
                //."loan_issue_date = '".$loanDate."',"
                //."loan_interest_percent = '".$interest."',"
                ."updated_date = now() "
                ."WHERE id = " . $ps_id . "";
                
        if(mysql_query($sql))
        {
            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = "Person's Scheme Updated Successfully";
            $this->session->setFlash("msg", $temp);
            return $id;
        }
        else
        {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = "Person's Scheme Not Updated";
            $this->session->setFlash("msg", $temp);
            return false;
        }
    }
    
    function removePS($ps_id){
        if(mysql_query('DELETE FROM person_scheme WHERE id = ' . $ps_id . ''))
            return true;
    }
	
	function removePSByPersonID($ps_id){
        if(mysql_query('DELETE FROM person_scheme WHERE person_id = ' . $ps_id . ''))
            return true;
    }
    
    function getPSInfoByID($ps_id){
        $sql = "SELECT * FROM person_scheme WHERE id = " . $ps_id;
        $res = mysql_query($sql);
        return mysql_fetch_assoc($res);
    }
	
    static function getPSInfoByIDstatic($ps_id){  //Vipul do not know static can be written or Not, so separate static method created by Vipul
        $sql = "SELECT * FROM person_scheme WHERE id = " . $ps_id;
        $res = mysql_query($sql);
        return mysql_fetch_assoc($res);
    }
	
	function getPSInfoByPersonID($ps_id){
        $sql = "SELECT scheme_id FROM person_scheme WHERE person_id = " . $ps_id;
        $res = mysql_query($sql);
		return $res;
    }
    
    function getAllPS(){
        $sql = "SELECT ps.*, p.person_name, s.scheme_name FROM person_scheme ps
                LEFT JOIN person p ON ps.person_id = p.id
                LEFT JOIN scheme s ON ps.scheme_id = s.id
                ORDER BY ps.created_date DESC";
        $res = mysql_query($sql);
        return $res;
    }
    
}