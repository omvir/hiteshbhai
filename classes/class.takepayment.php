<?php

class TakePayment
{
    private $session;
    
    function __construct()
    {
        $this->session = new SessionSet();
    }
    
    function addPersonTransaction($data){
        $scheme_ids = $data['hidden_scheme_id'];
        $isInstallment = $data['addInstallmentAmt'];
        $installment_amount = $data['addInstallmentTxt'];
				$fine_amount_array  = $data['addFineTxt'];
        $comments = $data['installmentComment'];
        
        foreach($scheme_ids as $key => $scheme_id){
            $psSchemeInfo = PersonToScheme::getPSInfoByIDstatic($scheme_id);
            
            $person_id = $psSchemeInfo['person_id'];
            $transaction_type = 'CR';
            
            if(isset($installment_amount[$key]) && $installment_amount[$key] != ""){
								$fine_amount_arrayToStore = $fine_amount_array[$key] != "" ? mysql_real_escape_string($fine_amount_array[$key]) : 0;
                $sql = "INSERT INTO scheme_installment SET person_scheme_id = '" . $psSchemeInfo['id'] . "',
                        installment_amount = " . mysql_real_escape_string($installment_amount[$key]) . ",
												fine_total         = " . $fine_amount_arrayToStore . ",
												is_paid            = 'YES',
                        comment = '" . mysql_real_escape_string($comments[$key]) . "',
                        created_date = now();";
                mysql_query($sql);
                
                // Update Person Scheme's Total Amount with Credited Payment
                
                mysql_query("UPDATE person_scheme SET total_amount = total_amount + " . mysql_real_escape_string($installment_amount[$key]) . ", updated_date = now() WHERE id = " . $scheme_id . "");
            }
        }
        
        $temp = array();
        $temp['type'] = 'success';
        $temp['text'] = "Payment Taken Successfully";
        $this->session->setFlash("msg", $temp);
    }
    
    function getPersonListBySchemeIdMonthAndYear($scheme_id, $month, $year){
        
        $sql = "SELECT ps.*, p.person_name, s.installment_amount, si.total_amount AS trans_amount FROM person_scheme ps
                LEFT JOIN person p ON ps.person_id = p.id
                LEFT JOIN scheme s ON ps.scheme_id = s.id
                LEFT JOIN scheme_installment si ON ps.id = si.person_scheme_id AND YEAR(si.created_date) = " . $year . " AND MONTH(si.created_date) = " . $month . "
                WHERE ps.scheme_id = " . $scheme_id . "
                GROUP BY ps.id";
        
        $res = mysql_query($sql);
        return $res;
    }
    
    function getPersonLoanList($scheme_id){
        $sql = "SELECT l.*, p.person_name, s.scheme_name, s.interest_fine_start_day, s.interest_fine_amount
                FROM loan l
                LEFT JOIN person p ON l.person_id = p.id
                LEFT JOIN scheme s ON l.scheme_id = s.id
                WHERE s.id = " . $scheme_id . "";
        
        $res = mysql_query($sql);
        return $res;
    }
    
}