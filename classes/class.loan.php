<?php

class Loan
{
    private $session;
    
    function __construct()
    {
        $this->session = new SessionSet();
    }
    
    function addLoan($data){
        
        $person_id = mysql_real_escape_string($data['person_id']);
        $scheme_id = mysql_real_escape_string($data['scheme_id']);
        $totalAmount = mysql_real_escape_string($data['totalAmount']);
        $interest = mysql_real_escape_string($data['interest']);
        
        $dayLoanDate = mysql_real_escape_string($data['dayLoanDate']);
        $monthLoanDate = mysql_real_escape_string($data['monthLoanDate']);
        $yearLoanDate = mysql_real_escape_string($data['yearLoanDate']);
        $loanDate = date('Y-m-d', strtotime($yearLoanDate . '-' . date('m', strtotime($monthLoanDate)) . '-' .  $dayLoanDate));
        
        $sql = "INSERT INTO loan SET "
                ."person_id = '".$person_id."',"
                ."scheme_id = '".$scheme_id."',"
                ."total_amount = '".$totalAmount."',"
                ."interest = '".$interest."',"
                ."loan_date = '".$loanDate."',"
                ."current_pending_amount = '".$totalAmount."',"
                ."created_date = now(),"
                ."updated_date = now()";
        mysql_query($sql);
        
        $id = mysql_insert_id();
        if($id > 0)
        {
            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = "Loan Added Successfully";
            $this->session->setFlash("msg", $temp);
            return $id;
        }
        else
        {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = "Loan Not Added";
            $this->session->setFlash("msg", $temp);
            return false;
        }
    }
    
    function updateLoan($data){
        $loan_id = mysql_real_escape_string($data['loan_id']);
        $person_id = mysql_real_escape_string($data['person_id']);
        $scheme_id = mysql_real_escape_string($data['scheme_id']);
        $totalAmount = mysql_real_escape_string($data['totalAmount']);
        $interest = mysql_real_escape_string($data['interest']);
        
        $dayLoanDate = mysql_real_escape_string($data['popupDayLoanDate']);
        $monthLoanDate = mysql_real_escape_string($data['popupMonthLoanDate']);
        $yearLoanDate = mysql_real_escape_string($data['popupYearLoanDate']);
        $loanDate = date('Y-m-d', strtotime($yearLoanDate . '-' . date('m', strtotime($monthLoanDate)) . '-' .  $dayLoanDate));
        
        $sql = "UPDATE loan SET "
                ."person_id = '".$person_id."',"
                ."scheme_id = '".$scheme_id."',"
                ."total_amount = '".$totalAmount."',"
                ."interest = '".$interest."',"
                ."loan_date = '".$loanDate."',"
                ."current_pending_amount = '".$totalAmount."',"
                ."updated_date = now() "
                ."WHERE id = " . $loan_id . "";
                
        if(mysql_query($sql))
        {
            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = "Loan Updated Successfully";
            $this->session->setFlash("msg", $temp);
            return $id;
        }
        else
        {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = "Loan Not Updated";
            $this->session->setFlash("msg", $temp);
            return false;
        }
    }
    
    function removeLoan($loan_id){
        if(mysql_query('DELETE FROM loan WHERE id = ' . $loan_id . ''))
            return true;
    }
    
    function getLoanInfoByID($loan_id){
        $sql = "SELECT * FROM loan WHERE id = " . $loan_id;
        $res = mysql_query($sql);
        return mysql_fetch_assoc($res);
    }
    
    function getLoanList(){
        $sql = "SELECT l.*, p.person_name, s.scheme_name FROM loan l
                LEFT JOIN person p ON l.person_id = p.id
                LEFT JOIN scheme s ON l.scheme_id = s.id
                ORDER BY l.created_date DESC";
        $res = mysql_query($sql);
        return $res;
    }
	function getInterestDetail()
	{	
		$loan_interest_id = $_GET['loan_id'];
		$sql = "SELECT interest_amount, due_date, receive_date, fine_amount_received FROM loan_interest 
		WHERE loan_id = " .$loan_interest_id. " AND is_paid = 'YES'";
		
		$res = mysql_query($sql);
		return $res;
	}
	function getLoanInterestPenalty($loan_id){
		//We are doing sum of interest_amount instead of interest_amount_received, because we want to sum of is_paid = 'YES'
        $sql = "SELECT l.id, SUM(li.interest_amount) as interest_recieved, SUM(li.fine_amount_received) as penalty_received FROM loan l
                LEFT JOIN loan_interest li ON l.id = li.loan_id
                WHERE li.loan_id = {$loan_id} AND li.is_paid = 'YES'
                GROUP BY li.loan_id
                ORDER BY l.created_date DESC";
        $res = mysql_query($sql);
		$result = array();
		while($record = mysql_fetch_assoc($res)) {
			$result = $record;
		}
        return $result;
    }
    
    function getLoanPayment($loan_id){
        
		$sql = "SELECT l.id, SUM(lc.loan_credit) as payment_received FROM loan l
                LEFT JOIN loan_credit lc ON l.id = lc.loan_id
                WHERE lc.loan_id = {$loan_id}
                GROUP BY lc.loan_id
                ORDER BY l.created_date DESC";
        $res = mysql_query($sql);
		$result = array();
		while($record = mysql_fetch_assoc($res)) {
			$result = $record;
		}
        return $result;
    }
    
}