<?php

class Person
{
    private $session;
    
    function __construct()
    {
        $this->session = new SessionSet();
    }
    
    function addPerson($data)
    {
        $person_name = mysql_real_escape_string($data['person_name']);
        $phone1 = mysql_real_escape_string($data['phone1']);
        $phone2 = mysql_real_escape_string($data['phone2']);
        
        $selectSql = "SELECT * FROM person WHERE person_name = '" . $person_name . "' AND (phone1 = '" . $phone1 . "' OR phone2 = '" . $phone2 . "')";
        
        $resultSql = mysql_query($selectSql);
        
        if(mysql_num_rows($resultSql) == 0){
        
            $sql = "INSERT INTO person SET "
                    ."person_name = '".$person_name."',"
                    ."phone1 = '".$phone1."',"
                    ."phone2 = '".$phone2."',"
                    ."created_date = now(),"
                    ."updated_date = now()";
            mysql_query($sql);

            $id = mysql_insert_id();
            if($id > 0)
            {
                $temp = array();
                $temp['type'] = 'success';
                $temp['text'] = 'Person Added Successfully';
                $this->session->setFlash("msg", $temp);
                return $id;
            }
            else
            {
                $temp = array();
                $temp['type'] = 'error';
                $temp['text'] = 'Person Not Added';
                $this->session->setFlash("msg", $temp);
                return false;
            }
        } else {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = 'Person Already Exist.';
            $this->session->setFlash("msg", $temp);
            return false;
        }
    }
    
    function updatePerson($data){
        
        $person_id = mysql_real_escape_string($data['person_id']);
        $person_name = mysql_real_escape_string($data['person_name']);
        $phone1 = mysql_real_escape_string($data['phone1']);
        $phone2 = mysql_real_escape_string($data['phone2']);
        
        $sql = "UPDATE person SET "
                ."person_name = '".$person_name."',"
                ."phone1 = '".$phone1."',"
                ."phone2 = '".$phone2."',"
                ."updated_date = now() "
                ."WHERE id = " . $person_id . "";
        
        if(mysql_query($sql)){
            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = 'Person Updated Successfully';
            $this->session->setFlash("msg", $temp);
            return $person_id;
        } else {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = 'Person Not Updated';
            $this->session->setFlash("msg", $temp);
            return false;
        }
    }
    
    function removePerson($person_id){
        if(mysql_query('DELETE FROM person WHERE id = ' . $person_id . ''))
            return true;
    }
    
    function getPersons(){
        $sql = "SELECT * FROM person ORDER BY created_date DESC";
        $res = mysql_query($sql);
        return $res;
    }
    
    function getPersonInfo($person_id){
        $sql = "SELECT * FROM person WHERE id = " . $person_id;
        $res = mysql_query($sql);
        return $res;
    }
    
    function getPersonsBySchemeId($scheme_id){
        $sql = "SELECT s.person_id, s.scheme_id, p.person_name FROM person_scheme s
                LEFT JOIN person p ON s.person_id = p.id
                WHERE s.scheme_id = " . $scheme_id . "
                ORDER BY p.person_name";
        $res = mysql_query($sql);
        return $res;
    }
}