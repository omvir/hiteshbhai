<?php

class LoanCredit
{
    private $session;
    
    function __construct()
    {
        $this->session = new SessionSet();
    }
    
    function addLoanCredit($data){
        $person_id = mysql_real_escape_string($data['person_id']);
        $scheme_id = mysql_real_escape_string($data['scheme_id']);
        
        $loan_id = $this->getLoanIdFromPersonAndSchemeIDs($person_id, $scheme_id);
        $credit_amount = mysql_real_escape_string($data['credit_amount']);
        $loanCredit_day = mysql_real_escape_string($data['dayLoanCreditDate']);
        $loanCredit_month = mysql_real_escape_string($data['monthLoanCreditDate']);
        $loanCredit_year = mysql_real_escape_string($data['yearLoanCreditDate']);
        $loanCredit_date = date('Y-m-d', strtotime($loanCredit_day . $loanCredit_month . $loanCredit_year));
        
        $sql = "INSERT INTO loan_credit SET
                loan_id = " . $loan_id . ",
                loan_credit = " . $credit_amount . ",
                loan_credit_date = '" . $loanCredit_date . "',
                created_date = now(),
                updated_date = now();";
        mysql_query($sql);
        
        $id = mysql_insert_id();
        if($id > 0)
        {
            $update_query = "UPDATE loan SET current_pending_amount = current_pending_amount - " . $credit_amount . "
                            WHERE person_id = " . $person_id . " AND scheme_id = " . $scheme_id . ";";
            mysql_query($update_query);
            
            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = 'Amount Credited Successfully';
            $this->session->setFlash("msg", $temp);
            return $id;
        }
        else
        {
            $temp = array();
            $temp['type'] = 'error';
            $temp['text'] = 'Amount Not Credited';
            $this->session->setFlash("msg", $temp);
            return false;
        }
        exit;
    }
    
    function getLoanCreditList(){
	
        $sql = "SELECT lc.*, p.person_name, s.scheme_name, l.loan_date,
                l.total_amount, l.interest
                FROM loan_credit lc
                LEFT JOIN loan l ON lc.loan_id = l.id
                LEFT JOIN person p ON l.person_id = p.id
                LEFT JOIN scheme s ON l.scheme_id = s.id
                ORDER BY lc.created_date DESC";
        $res = mysql_query($sql);
        return $res;
    }
    function getCreditedDetail()
	{
		//$loan_credit_id = mysql_real_escape_string($_GET['loan_id']);
		$loan_credit_id = $_GET['loan_id'];
		$sql = "SELECT loan_credit, loan_credit_date FROM loan_credit
		WHERE loan_id = " .$loan_credit_id. " ";
		
		$res = mysql_query($sql);
		return $res;
	}
    function getLoanIdFromPersonAndSchemeIDs($person_id, $scheme_id){
        $sql = "SELECT id FROM loan
                WHERE person_id = " . $person_id . "
                AND scheme_id = " . $scheme_id . ";";
        $res = mysql_query($sql);
        $array = mysql_fetch_assoc($res);
        return $array['id'];
    }
    
}