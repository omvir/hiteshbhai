<?php

class Login {

    private $session;

    function __construct() {
        $this->session = new SessionSet();
    }

    function verifyAdminLogin($data) {
        $username = mysql_real_escape_string($data['uname']);
        $password = mysql_real_escape_string($data['pass']);
        $password = md5($password);

        $sql = "SELECT * FROM user WHERE username = '" . $username . "' AND password = '" . $password . "'";
        $res = mysql_query($sql);

        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_assoc($res);
            $temp = array();
            $temp['u_id'] = $row['id'];
            $temp['u_name'] = $row['name'];
            $temp['u_email'] = $row['email'];
            $temp['u_role'] = $row['role'];

            if ($row['role'] == "admin")
                $this->session->setSession("admin_logged_in", $temp);
            else
                $this->session->setSession("user_logged_in", $temp);

            $temp = array();
            $temp['type'] = 'success';
            $temp['text'] = 'Logged In Successful';
            //$this->session->setFlash("msg", $temp);

            header('location: dashboard.php');
            exit;
        }
        else {
            return false;
        }
    }

    function logout() {
        unset($_SESSION);
        session_destroy();
        header("location:index.php");
        exit;
    }

}