<?php

class SessionSet
{
    function __construct()
    {
        
    }
    
    /*
     *  Here the function loginCheck is used for to check user is already logged in or not.
     *  Params: 
     *  $check_var = For which session variable you want to check.
     *  $page = For which page you want to check.(Page name must be 'login' for login page)
     *  $redirect_url1 = If user is already logged in then redirect to this page.
     *  $redirect_url2 = If user is not logged in then redirect to this page(Login page).
     */
    function loginCheck($check_var,$page,$redirect_url1 = "dashboard.php",$redirect_url2 = "index.php")
    {
        if(strtolower($page) == 'login')
        {
            if(isset($_SESSION[$check_var]) && !empty($_SESSION[$check_var]))
            {
                header("location:".$redirect_url1);exit;
            }
            else
            {
                return true;
            }
        }
        else
        {
            if(isset($_SESSION[$check_var]) && !empty($_SESSION[$check_var]))
            {
                return true;
            }
            else
            {
                header("location:".$redirect_url2);exit;
            }
        }
    }
    
    function setFlash($varName,$value)
    {
        $_SESSION[$varName] = $value;
    }
    
    function getFlash($varName)
    {
		$temp = array();
		if(isset($_SESSION[$varName]))
		{
			$temp = $_SESSION[$varName];
		}
		else
		{
			$temp = NULL;
		}
        unset($_SESSION[$varName]);
		return $temp;
    }
    
    function setSession($varName,$value)
    {
        $_SESSION[$varName] = $value;
    }
    
    function getSession($varName)
    {
        return $_SESSION[$varName];
    }
}

$session = new SessionSet();