<?php

class Database
{
    private $host;
    private $user;
    private $pass;
    private $dbname;
    private $conn;
    
    public function __construct($host,$user,$pass,$dbname)
    {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->dbname = $dbname;
        
        $this->db_connect();
        $this->db_select();
    }
    
    public function db_connect()
    {
        $this->conn = mysql_connect($this->host,  $this->user,  $this->pass) or die("Mysql Connection Error");
    }
    
    public function db_select()
    {
        mysql_select_db($this->dbname,  $this->conn) or die("Database Not Found");
    }
}

$db = new Database(HOST,USER,PASSWORD,DB_NAME);

if(isset($_SESSION['admin_logged_in']['u_id']) && $_SESSION['admin_logged_in']['u_id'] != ""){
    $user_id = $_SESSION['admin_logged_in']['u_id'];
} else {
    $user_id = 1;
}

$selDefaultScheme = mysql_query("SELECT default_scheme_id FROM user WHERE id = " . $user_id);

if(mysql_num_rows($selDefaultScheme)){
    $selDefaultSchemeRes = mysql_fetch_assoc($selDefaultScheme);
    define('DEFAULT_SCHEME_ID', $selDefaultSchemeRes['default_scheme_id']);
} else {
    define('DEFAULT_SCHEME_ID', '0');
}
