<?php
define("DEF", 1);
require_once 'config.php';
require_once CLASSPATH.'class.user.php';

$page = 'changepassword';
$session->loginCheck("admin_logged_in",$page);

$user = new User();

if(isset($_POST['submit']))
{
    $user->changePassword($_POST);
}

include_once 'includes/header.php';
?>
<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>

            <aside class="right-side">
                <section class="content-header">
                    <h1>Change Password</h1>
                    <ol class="breadcrumb">
                        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Change Password</li>
                    </ol>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <form action="#" method="post">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Old Password</label>
                                            <input type="password" class="form-control" id="oldpass" name="oldpass" placeholder="Enter Old Password">
                                        </div>
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" class="form-control" id="newpass" name="newpass" placeholder="Enter Password">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control" id="confpass" name="confpass" placeholder="Confirm Password">
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" name="submit" class="btn btn-primary">Change Password</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </aside>
        </div>
    </body>
</html>
