<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH.'class.scheme.php';

$page = 'scheme';
$session->loginCheck("admin_logged_in",$page);


$scheme = new Scheme();

if(isset($_POST['submit']))
{
    $scheme_id = $scheme->addScheme($_POST);
}

include_once 'includes/header.php';
?>
<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Scheme</h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Home </li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <form action="" method="post">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Scheme Name</label>
                                            <input type="text" class="form-control" id="scheme" name="scheme" placeholder="Scheme Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Installment Amount</label>
                                            <input type="text" class="form-control" id="inst_amount" name="inst_amount" placeholder="Installment Amount">
                                        </div>
                                        <div class="form-group">
                                            <label>Installment Fine Start</label>
                                            <input type="text" class="form-control" id="inst_fineday" name="inst_fineday" placeholder="Fine Start Day">
                                        </div>
                                        <div class="form-group">
                                            <label>Installment Fine Amount</label>
                                            <input type="text" class="form-control" id="inst_fine_amount" name="inst_fine_amount" placeholder="Installment Fine Amount">
                                        </div>
                                        <div class="form-group">
                                            <label>Interest Fine Start</label>
                                            <input type="text" class="form-control" id="interest_fineday" name="interest_fineday" placeholder="Interest Fine Start Day">
                                        </div>
                                        <div class="form-group">
                                            <label>Interest Fine Amount</label>
                                            <input type="text" class="form-control" id="interest_fine_amount" name="interest_fine_amount" placeholder="Interest Fine Amount">
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" name="submit" class="btn btn-primary" value="add">Add Scheme</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

    </body>
</html>
