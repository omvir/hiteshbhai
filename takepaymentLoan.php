<?php
define("DEF", 1);
$page = 'loan';
require_once 'config.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.persontoscheme.php';
include_once CLASSPATH.'class.takepaymentLoan.php';

$scheme = new Scheme();
$takepayment = new TakePayment();

if(isset($_POST['submit']))
{
    $takepayment->addPersonLoanInterestAndFine($_POST);
}

$scheme_res = $scheme->getSchemes();

include_once 'includes/header.php';
?>

<link href="<?php echo BASEURL; ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo BASEURL; ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script src="<?php echo BASEURL; ?>js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
var datatable;
window.onload=function(){
    populatedropdown("dayTakePaymentDate", "monthTakePaymentDate", "yearTakePaymentDate", false);
}

$(document).ready(function(){
    
    var dataTableOptions = {"bPaginate": false,
                            "bLengthChange": false,
                            "bFilter": false,
                            "bSort": false,
                            "bInfo": false,
                            "bAutoWidth": false,
                            "destroy": true
                            };
    
    datatable = $('#tbl_takePaymentList').dataTable(dataTableOptions);
    
    $('#getPersonLoanList').click(function(){
        $('#till_date_content').html('');
        
        $.ajax({
            type: 'POST',
            url: './ajax/takepayment_listLoan.php',
            data: $('#paymentListForm').serializeArray(),
            beforeSend: function(){
                $('#takePaymentList .box').append('<div class="overlay"></div><div class="loading-img"></div>');
                datatable.fnClearTable();
                datatable.fnDestroy();
            },
            success: function(response){
                $('#takePaymentList .overlay, #takePaymentList .loading-img').remove();
                
                if(response != ""){
                    $('#tbl_takePaymentList tbody').html(response);
                    $('#till_date_content').html($('#till_date').val());
                    $('#post_date_content').val($('#post_date').val());
                }
                
                datatable.dataTable(dataTableOptions);
                fillAmounts();
            }
        });
    });
});

function fillAmounts(){
    $('.intFineCheck').click(function(){
        var parent = $(this).parents('tr');
        $(parent).find('.input_pending_interest').val($(parent).find('.pending_interest').html());
        $(parent).find('.input_pending_fine').val($(parent).find('.pending_fine').html());
        $(parent).find('.column_total').html($(parent).find('.pending_total').html());
    });
}

function intFineCheck(obj) {
    $(obj).parents('tr').find('.column_total').html('0.00');
}

function addTotal(obj){
    var totalSum = 0;
    
    $(obj).parents('tr').find('input[type="text"]').each(function(){
        if($(this).val() != "" && $(this).val() > 0){
            totalSum += parseFloat($(this).val());
        } else {
            totalSum += parseFloat(0);
        }
    });
    
    $(obj).parents('tr').find('.column_total').html(totalSum.toFixed(2));
}

function addInstallmentAmt(obj){
    var parentObj = $(obj).parent().parent();
    var installment_amt = parentObj.find('.installment_amt').text();
    parentObj.find('.installmentText').val(installment_amt);
}

</script>

<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Loan Payment</h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Home </li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <form id="paymentListForm" action="" method="post">
                                <div class="box-body clearfix">
                                    
                                    <div class="form-group col-md-3">
                                        <label>Scheme</label>
                                        <select class="form-control" id="scheme_id" name="scheme_id" required>
                                            <option value="">Select Scheme</option>
                                            <?php while($scheme_row = mysql_fetch_assoc($scheme_res)){ ?>
                                            <?php if($scheme_row['id'] == DEFAULT_SCHEME_ID) { ?>
                                                <option value="<?php echo $scheme_row['id']; ?>" selected="selected"><?php echo $scheme_row['scheme_name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $scheme_row['id']; ?>"><?php echo $scheme_row['scheme_name']; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-md-4">
                                        <label>Date</label>
                                        <div class="clearfix">
                                            <div class="col-md-4"><select id="dayTakePaymentDate" name="dayTakePaymentDate" class="form-control"></select></div>
                                            <div class="col-md-4"><select id="monthTakePaymentDate" name="monthTakePaymentDate" class="form-control"></select></div>
                                            <div class="col-md-4"><select id="yearTakePaymentDate" name="yearTakePaymentDate" class="form-control"></select></div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-1">
                                        <label>&nbsp;</label>
                                        <button id="getPersonLoanList" type="button" name="submit" class="form-control btn btn-primary" value="add">Go</button>
                                    </div>
                                    
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <div id="takePaymentList" class="row">
                        <div class="col-md-12">
                            <div class="box box-danger">
                                <form id="paymentListDataForm" action="" method="post">
                                <div class="box-body table-responsive">
                                    <input type="hidden" name="post_date_content" id="post_date_content" />
                                    <table id="tbl_takePaymentList" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Person Name</th>
                                                <th rowspan="2">Loan Date</th>
                                                <th rowspan="2">Loan Amount<br /> & <br />Interest %</th>
                                                <th rowspan="2">Current<br />Pending<br />Amount</th>
                                                <td colspan="3" align="center"><b>Debit Till Date<br /><span id="till_date_content"></span></b></td>
                                                <td colspan="5" align="center"><b>Credit</b></td>
                                            </tr>
                                            <tr>
                                                <th>Interest</th>
                                                <th>Penalty</th>
                                                <th>Total</th>
                                                <th>&nbsp;</th>
                                                <th>Interest</th>
                                                <th>Penalty</th>
                                                <th>Loan</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    
                                    <div class="box-footer">
                                        <button type="submit" name="submit" class="btn btn-success" value="add">Add Payment</button>
                                    </div>
                                    
                                </div>
                                
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </section><!-- /.content -->
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->

    </body>
</html>