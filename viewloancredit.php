<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH.'class.takepaymentLoan.php';

$page = 'loan';
$session->loginCheck("admin_logged_in",$page);

$loan = new TakePayment();

$loan_res = $loan->getLoanCreditInfo();

include_once 'includes/header.php';
?>
<!-- Here you can add extra css and js plugins -->
</head>
<body class="skin-blue">
    <?php include_once 'includes/top-block.php'; ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <?php include_once 'includes/sidebar.php'; ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Loan</h1>
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i> Home </li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Loan Interest Table</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="tbl_schemelist" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Loan ID</th>
                                    <th>Loan Credit</th>
                                    <th>Loan Credit Date</th>
                                    <th>Created Date</th>
                                    <th>Updated Date</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                while($loan_row = mysql_fetch_assoc($loan_res))
                                {
                                ?>
                                <tr>
                                    <td><?php echo $loan_row['loan_id']; ?></td>
                                    <td><?php echo $loan_row['loan_credit']; ?></td>
                                    <td><?php echo date('d F,Y',strtotime($loan_row['loan_credit_date'])); ?></td>
                                    <td><?php echo date('d F,Y',strtotime($loan_row['created_date'])); ?></
                                    <td><?php echo date('d F,Y',strtotime($loan_row['updated_date'])); ?></td>
                                    <!-- <td></td> -->
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </section>
        </aside>
    </div>

</body>
</html>
