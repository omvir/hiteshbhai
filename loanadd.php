<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.loan.php';

$page = 'loan';
$todayDate = date('d-m-Y');
$scheme = new Scheme();
$loan = new Loan();

if(isset($_POST['submit']))
{
    $loan->addLoan($_POST);
}

$scheme_res = $scheme->getSchemes();
$loan_res = $loan->getLoanList();

include_once 'includes/header.php';
?>
<link href="<?php echo BASEURL; ?>css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

<link href="<?php echo BASEURL; ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo BASEURL; ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<link href="<?php echo BASEURL; ?>css/bootstrap-validator/bootstrap.validator.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/bootstrap-validator/bootstrap.validator.min.js" type="text/javascript"></script>

<script src="<?php echo BASEURL; ?>js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    
window.onload=function(){
    populatedropdown("dayLoanDate", "monthLoanDate", "yearLoanDate", false);
}

$(document).ready(function() {
    $('#tbl_loanList').dataTable({
        "iDisplayLength":500,
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        /* set up row grouping */
            "fnDrawCallback": function (oSettings) {
            if (oSettings.aiDisplay.length == 0) {
                return;
            }

            var nTrs = $('.table tbody tr');
            var iColspan = nTrs[0].getElementsByTagName('td').length;
            var sLastGroup = "";
            for (var i = 0; i < nTrs.length; i++) {
                $(".footer-total-amount-total").html( parseInt($(".footer-total-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-total-amount').html()));
                $(".footer-interest-amount-total").html( parseInt($(".footer-interest-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-interest-amount').html()));
                $(".footer-penalty-amount-total").html( parseInt($(".footer-penalty-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-penalty-amount').html()));
                $(".footer-payment-amount-total").html( parseInt($(".footer-payment-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-payment-amount').html()));
                $(".footer-current-amount-total").html( parseInt($(".footer-current-amount-total").html()) + parseInt($(nTrs[i]).find('.footer-current-amount').html()));
            }
        },
    });
    
    $('[data-toggle="modal"]').click(function(e) {
        e.preventDefault();
        var ps_id = $(this).attr('id').replace('loanId_', '');
        
        var url = './ajax/loan_edit.php?loan_id=' + ps_id;
        
        if (url.indexOf('#') == 0) {
            $('#loanModal').modal('show');}
        else {
            $.get(url, function (data) {
                $('#loanModal .te').html(data);
                $('#loanModal').modal();
            }).success(function () {
                $('input:text:visible:first').focus();
            });
        }
    });
    
    $('.launchConfirm').on('click', function (e) {
        e.preventDefault();
        $obj = $(this);
        $('#confirm').modal().one('click', '#delete', function (e) {
            var ps_id = $obj.attr('id').replace('loanId_', '');
            var url = './ajax/loan_remove.php?&loan_id=' + ps_id;
            $.get(url, function (data) {
                if(data == 'true')
                    window.location.href = window.location.href;
                else{
                    $('#confirm').modal('show');
                    $('#confirm .te').html("<p class='text-red'>Loan not deleted! Please try again.</p>");
                }
            });
        });
    });
    
    getSchemePersons($('#scheme_id'));
    
    $('#scheme_id').change(function(){
        getSchemePersons($(this));
    });
});

function getSchemePersons(obj){
    $.ajax({
        type: 'post',
        url: './ajax/get_persons_by_scheme.php',
        data: { scheme_id : $(obj).val()},
        beforeSend: function(){
            $('#person_id').attr({'disabled' : 'disabled'});
        },
        success: function(data){
            $('#person_id').removeAttr('disabled');
            $('#person_id').html(data);
        }
    });
}
</script>
<style>
	.table > tbody > tr > td{padding: 5px;}
</style>
<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Add Loan</h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Home </li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <form id="personForm" action="" method="post">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label>Scheme</label>
                                                <select class="form-control" id="scheme_id" name="scheme_id" required>
                                                    <option value="">Select Scheme</option>
                                                    <?php while($scheme_row = mysql_fetch_assoc($scheme_res)){ ?>
                                                    <?php if($scheme_row['id'] == DEFAULT_SCHEME_ID) { ?>
                                                        <option value="<?php echo $scheme_row['id']; ?>" selected="selected"><?php echo $scheme_row['scheme_name']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $scheme_row['id']; ?>"><?php echo $scheme_row['scheme_name']; ?></option>
                                                    <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-md-3">
                                                <label>Person</label>
                                                <select class="form-control" id="person_id" name="person_id" required>
                                                    <option value="">Select Person</option>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-md-2">
                                                <label>Total Amount</label>
                                                <input type="text" class="form-control" id="totalAmount" name="totalAmount" placeholder="Total Amount" required pattern="\d+(\.\d{2})?">
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            
                                            <div class="form-group col-md-2">
                                                <label>Interest %</label>
                                                <input type="text" class="form-control" id="interest" name="interest" placeholder="%" required />
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label>Loan Date</label>
                                                <div class="clearfix">
                                                    <div class="col-md-4"><select id="dayLoanDate" name="dayLoanDate" class="form-control"></select></div>
                                                    <div class="col-md-4"><select id="monthLoanDate" name="monthLoanDate" class="form-control"></select></div>
                                                    <div class="col-md-4"><select id="yearLoanDate" name="yearLoanDate" class="form-control"></select></div>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label>&nbsp;</label>
                                                <button id="submit_person" type="submit" name="submit" class="form-control btn btn-primary" value="add">Add Loan</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Loan List</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <table id="tbl_loanList" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Person Name</th>
                                                <th>Scheme Name</th>
                                                <th>Total Amount</th>
                                                <th>Interest %</th>
                                                <th>Loan Date</th>
                                                <th>Interest Recieved</th>
                                                <th>Penalty Recieved</th>
                                                <th>Payment Recieved</th>
                                                <th>Current Pending Amount</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while($loan_data = mysql_fetch_assoc($loan_res)) {
                                            	$amout = $loan->getLoanInterestPenalty($loan_data['id']);
												$payment = $loan->getLoanPayment($loan_data['id']);
                                            ?>
                                            <?php // echo "<pre>";print_r($amout);exit; ?>
                                            <tr>
                                                <td><?php echo $loan_data['person_name']; ?></td>
                                                <td><?php echo $loan_data['scheme_name']; ?></td>
                                                <td class="footer-total-amount" align="right"><?php if($loan_data['total_amount'] != ""){ echo $loan_data['total_amount']; }else { echo "0"; } ?></td>
                                                <td align="right"><?php echo $loan_data['interest']; ?></td>
                                                <td><?php echo date('d F, Y',strtotime($loan_data['loan_date'])); ?></td>
                                                <td align="right"><a href="interestList.php?loan_id=<?php echo $loan_data['id']; ?>"><span class="footer-interest-amount"><?php if(isset($amout['interest_recieved']) && $amout['interest_recieved'] != ""){ echo $amout['interest_recieved']; }else{ echo "0"; } ?></span></a></td>
                                                <td align="right"><a href="interestList.php?loan_id=<?php echo $loan_data['id']; ?>"><span class="footer-penalty-amount"><?php if(isset($amout['penalty_received']) && $amout['penalty_received'] != ""){ echo $amout['penalty_received']; }else{ echo "0"; } ?></a></span></td>
                                                <td class="footer-payment-amount" align="right"><?php if(isset($payment['payment_received']) && $payment['payment_received'] != ""){ echo $payment['payment_received']; }else{ echo "0"; } ?></td>
                                                <td align="right"><a href="creditedList.php?loan_id=<?php echo $loan_data['id']; ?>"><span class="footer-current-amount"><?php if($loan_data['current_pending_amount'] != ""){ echo $loan_data['current_pending_amount']; }else{ echo "0"; } ?></a></span></td>
                                                <td>
                                                    <a id="loanId_<?php echo $loan_data['id']; ?>" class="input-mini btn btn-warning" data-toggle="modal">
                                                       <i class="glyphicon glyphicon-edit"></i>
                                                   </a>
                                                   <a id="loanId_<?php echo $loan_data['id']; ?>" class="launchConfirm input-mini btn btn-danger">
                                                       <i class="glyphicon glyphicon-remove"></i>
                                                   </a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                        	<th style="text-align: right;" colspan="2">Total</th>
                                            <th class="footer-total-amount-total" style="text-align: right;">0</th>
                                            <th colspan="2"></th>
                                            <th class="footer-interest-amount-total" style="text-align: right;">0</th>
                                            <th class="footer-penalty-amount-total" style="text-align: right;">0</th>
                                            <th class="footer-payment-amount-total" style="text-align: right;">0</th>
                                            <th class="footer-current-amount-total" style="text-align: right;">0</th>
                                            <th></th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
        </div><!-- ./wrapper -->
        
        <!-- /.modal -->
        <div class="modal fade" id="loanModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Loan</h4>
                    </div>
                    <div class="modal-body"><div class="te">Please wait...</div></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.href = window.location.href;">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        <div id="confirm" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        Are you sure?
                        <div class="te"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
                        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
