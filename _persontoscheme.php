<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH.'class.person.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.persontoscheme.php';

$page = 'persontoscheme';
$todayDate = date('d-m-Y');
$person = new Person();
$scheme = new Scheme();
$personToScheme = new PersonToScheme();

if(isset($_POST['submit']))
{
    $personToScheme->addPersonScheme($_POST);
}

$person_res = $person->getPersons();
$scheme_res = $scheme->getSchemes();
$personScheme_res = $personToScheme->getAllPS();

include_once 'includes/header.php';
?>
<link href="<?php echo BASEURL; ?>css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

<link href="<?php echo BASEURL; ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo BASEURL; ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<link href="<?php echo BASEURL; ?>css/bootstrap-validator/bootstrap.validator.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/bootstrap-validator/bootstrap.validator.min.js" type="text/javascript"></script>

<script src="<?php echo BASEURL; ?>js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    
window.onload=function(){
    populatedropdown("dayLoanDate", "monthLoanDate", "yearLoanDate", false);
}

$(document).ready(function() {
    $('#tbl_personSchemeList').dataTable({
        "iDisplayLength":500,
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
    });
    
    $('[data-toggle="modal"]').click(function(e) {
        e.preventDefault();
        var ps_id = $(this).attr('id').replace('psId_', '');
        
        var url = './ajax/persontoscheme_edit.php?ps_id=' + ps_id;
        
        if (url.indexOf('#') == 0) {
            $('#psModal').modal('show');}
        else {
            $.get(url, function (data) {
                $('#psModal .te').html(data);
                $('#psModal').modal();
            }).success(function () {
                $('input:text:visible:first').focus();
            });
        }
    });
    
    $('.launchConfirm').on('click', function (e) {
        e.preventDefault();
        $obj = $(this);
        $('#confirm').modal().one('click', '#delete', function (e) {
            var ps_id = $obj.attr('id').replace('psId_', '');
            var url = './ajax/persontoscheme_remove.php?&ps_id=' + ps_id;
            $.get(url, function (data) {
                if(data == 'true')
                    window.location.href = window.location.href;
                else{
                    $('#confirm').modal('show');
                    $('#confirm .te').html("<p class='text-red'>Person's Scheme not deleted! Please try again.</p>");
                }
            });
        });
    });
});
</script>

<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Add Loan</h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Home </li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <form id="personForm" action="" method="post">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label>Person</label>
                                                <select class="form-control" id="person_id" name="person_id" required>
                                                    <option value="">Select Person</option>
                                                    <?php while($person_row = mysql_fetch_assoc($person_res)){ ?>

                                                    <option value="<?php echo $person_row['id']; ?>"><?php echo $person_row['person_name']; ?></option>

                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Scheme</label>
                                                <select class="form-control" id="scheme_id" name="scheme_id" required>
                                                    <option value="">Select Scheme</option>
                                                    <?php while($scheme_row = mysql_fetch_assoc($scheme_res)){ ?>

                                                    <option value="<?php echo $scheme_row['id']; ?>"><?php echo $scheme_row['scheme_name']; ?></option>

                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label>Loan Amount</label>
                                                <input type="text" class="form-control" id="loanAmount" name="loanAmount" placeholder="Loan Amount" required pattern="\d+(\.\d{2})?">
                                            </div>
                                        </div>
                                        
                                        <div class="row">

                                            <div class="form-group col-md-4">
                                                <label>Loan Date</label>
                                                <div class="clearfix">
                                                    <div class="col-md-4"><select id="dayLoanDate" name="dayLoanDate" class="form-control"></select></div>
                                                    <div class="col-md-4"><select id="monthLoanDate" name="monthLoanDate" class="form-control"></select></div>
                                                    <div class="col-md-4"><select id="yearLoanDate" name="yearLoanDate" class="form-control"></select></div>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label>Interest %</label>
                                                <input type="text" class="form-control" id="interest" name="interest" placeholder="%" required />
                                            </div>
                                            
                                            <div class="form-group col-md-2">
                                                <label>&nbsp;</label>
                                                <button id="submit_person" type="submit" name="submit" class="form-control btn btn-primary" value="add">Add Loan</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Loan List</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <table id="tbl_personSchemeList" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Person Name</th>
                                                <th>Scheme Name</th>
                                                <th>Loan Amount</th>
                                                <th>Loan Date</th>
                                                <th>Interest %</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while($personScheme = mysql_fetch_assoc($personScheme_res)) { ?>
                                            <tr>
                                                <td><?php echo $personScheme['person_name']; ?></td>
                                                <td><?php echo $personScheme['scheme_name']; ?></td>
                                                <td><?php echo $personScheme['loan_amount']; ?></td>
                                                <td><?php echo date('d F, Y',strtotime($personScheme['loan_issue_date'])); ?></td>  
                                                <td><?php echo $personScheme['loan_interest_percent']; ?></td>
                                                <td>
                                                    <a id="psId_<?php echo $personScheme['id']; ?>" class="input-mini btn btn-warning" data-toggle="modal">
                                                       <i class="glyphicon glyphicon-edit"></i>
                                                   </a>
                                                   <a id="psId_<?php echo $personScheme['id']; ?>" class="launchConfirm input-mini btn btn-danger">
                                                       <i class="glyphicon glyphicon-remove"></i>
                                                   </a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
        </div><!-- ./wrapper -->
        
        <!-- /.modal -->
        <div class="modal fade" id="psModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Person's Scheme</h4>
                    </div>
                    <div class="modal-body"><div class="te">Please wait...</div></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.href = window.location.href;">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        <div id="confirm" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        Are you sure?
                        <div class="te"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
                        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
