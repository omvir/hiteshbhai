-- phpMyAdmin SQL Dump
-- version 4.3.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 24, 2014 at 01:04 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hiteshbhai`
--

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE IF NOT EXISTS `loan` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `scheme_id` int(11) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `loan_date` datetime NOT NULL,
  `current_pending_amount` decimal(10,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`id`, `person_id`, `scheme_id`, `total_amount`, `interest`, `loan_date`, `current_pending_amount`, `created_date`, `updated_date`) VALUES
(6, 28, 1, '160000.00', '2.00', '2014-11-10 00:00:00', '155562.00', '2014-11-14 20:32:38', '2014-11-14 20:32:38'),
(9, 1, 3, '100.00', '2.00', '2014-10-02 00:00:00', '42.00', '2014-11-22 09:43:36', '2014-11-22 09:43:36'),
(10, 1, 3, '100.00', '2.00', '2014-10-01 00:00:00', '100.00', '2014-11-27 09:57:27', '2014-11-27 09:57:27'),
(11, 22, 1, '5000.00', '2.00', '2014-10-10 00:00:00', '5000.00', '2014-11-27 19:33:10', '2014-11-27 19:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `loan_credit`
--

CREATE TABLE IF NOT EXISTS `loan_credit` (
  `id` int(11) NOT NULL,
  `loan_id` int(11) NOT NULL,
  `loan_credit` double(10,2) NOT NULL,
  `loan_credit_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan_credit`
--

INSERT INTO `loan_credit` (`id`, `loan_id`, `loan_credit`, `loan_credit_date`, `created_date`, `updated_date`) VALUES
(1, 6, 5000.00, '2014-11-14', '2014-11-14 20:34:37', '2014-11-14 20:34:37'),
(2, 9, 58.00, '2014-11-27', '2014-11-27 09:58:26', '2014-11-27 09:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `loan_interest`
--

CREATE TABLE IF NOT EXISTS `loan_interest` (
  `id` int(11) NOT NULL,
  `loan_id` int(11) NOT NULL,
  `loan_pending_amount` decimal(10,2) NOT NULL,
  `interest_amount` decimal(10,2) NOT NULL,
  `is_paid` enum('YES','NO') NOT NULL,
  `due_date` date NOT NULL,
  `interest_amount_received` decimal(10,2) NOT NULL,
  `fine_amount_received` decimal(10,2) NOT NULL,
  `receive_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan_interest`
--

INSERT INTO `loan_interest` (`id`, `loan_id`, `loan_pending_amount`, `interest_amount`, `is_paid`, `due_date`, `interest_amount_received`, `fine_amount_received`, `receive_date`) VALUES
(1, 7, '0.00', '0.00', 'YES', '2014-11-14', '0.00', '0.00', '2014-11-14 20:41:40'),
(41, 6, '155456.00', '3.00', 'YES', '2014-12-11', '0.00', '50.00', '2014-11-21 21:48:56'),
(42, 7, '92000.00', '1.00', 'YES', '2014-12-11', '0.00', '50.00', '2014-11-21 21:48:56'),
(43, 8, '182703.00', '3.00', 'YES', '2014-12-11', '0.00', '50.00', '2014-11-21 21:48:56'),
(59, 6, '155459.00', '3.00', 'YES', '2014-12-11', '0.00', '100.00', '2014-11-21 21:51:48'),
(60, 7, '92001.00', '1.00', 'YES', '2014-12-11', '0.00', '100.00', '2014-11-21 21:51:48'),
(61, 8, '182706.00', '3.00', 'YES', '2014-12-11', '0.00', '100.00', '2014-11-21 21:51:48'),
(74, 9, '100.00', '2.00', 'YES', '2014-11-11', '2.00', '340.00', '2014-11-27 09:58:26'),
(115, 9, '42.00', '0.84', 'NO', '2014-12-11', '0.00', '0.00', '0000-00-00 00:00:00'),
(116, 10, '100.00', '2.00', 'NO', '2014-11-11', '0.00', '0.00', '0000-00-00 00:00:00'),
(117, 10, '100.00', '2.00', 'NO', '2014-12-11', '0.00', '0.00', '0000-00-00 00:00:00'),
(128, 6, '155562.00', '3.00', 'NO', '2014-12-11', '0.00', '0.00', '0000-00-00 00:00:00'),
(129, 11, '5000.00', '100.00', 'NO', '2014-11-11', '0.00', '0.00', '0000-00-00 00:00:00'),
(130, 11, '5000.00', '100.00', 'NO', '2014-12-11', '0.00', '0.00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL,
  `person_name` varchar(255) NOT NULL,
  `phone1` varchar(15) NOT NULL,
  `phone2` varchar(15) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `person_name`, `phone1`, `phone2`, `created_date`, `updated_date`) VALUES
(1, 'Test', '235689', '8956223', '2014-10-10 00:09:36', '2014-10-10 00:09:36'),
(2, 'hitesh thumar', '', '', '2014-10-10 19:28:30', '2014-10-10 19:28:30'),
(3, 'dharmesh thumar', '', '', '2014-10-10 19:32:13', '2014-10-10 19:32:13'),
(4, 'ashvin raiyani', '', '', '2014-10-10 19:32:32', '2014-10-10 19:32:32'),
(5, 'haresh raiyani', '', '', '2014-10-10 19:32:41', '2014-10-10 19:32:41'),
(6, 'savan ramani', '', '', '2014-10-10 19:32:52', '2014-10-10 19:32:52'),
(7, 'ashokbhai ramani', '', '', '2014-10-10 19:33:12', '2014-10-10 19:33:12'),
(8, 'alpesh raiyani', '', '', '2014-10-10 19:40:14', '2014-10-10 19:40:14'),
(9, 'dharmesh raiyani', '', '', '2014-10-10 19:40:26', '2014-10-10 19:40:26'),
(10, 'chirag vadodariya', '', '', '2014-10-10 19:40:54', '2014-10-10 19:40:54'),
(11, 'sagar vadodariya', '', '', '2014-10-10 19:41:11', '2014-10-10 19:41:11'),
(12, 'prinse vadodariya', '', '', '2014-10-10 19:41:35', '2014-10-10 19:41:35'),
(13, 'rasik mendapara', '', '', '2014-10-10 19:42:10', '2014-10-10 19:42:10'),
(17, 'ashok singala', '', '', '2014-10-10 19:43:27', '2014-10-10 19:43:27'),
(18, 'mansukhbhai singala', '', '', '2014-10-10 19:43:38', '2014-10-10 19:43:48'),
(20, 'chirag mungara', '', '', '2014-10-10 19:44:49', '2014-10-10 19:45:06'),
(21, 'haresh raiyani G', '', '', '2014-10-10 19:47:20', '2014-10-10 19:49:48'),
(22, 'hitesh thumar G', '', '', '2014-10-10 19:47:33', '2014-10-10 19:49:32'),
(23, 'chirag vadodariya G', '', '', '2014-10-10 19:47:48', '2014-10-10 19:49:08'),
(25, 'ashvin raiyani G', '', '', '2014-10-10 19:50:16', '2014-10-10 19:50:16'),
(28, 'dharmesh thumar G', '', '', '2014-10-10 19:51:05', '2014-11-29 14:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `person_scheme`
--

CREATE TABLE IF NOT EXISTS `person_scheme` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `scheme_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_scheme`
--

INSERT INTO `person_scheme` (`id`, `person_id`, `scheme_id`, `created_date`, `updated_date`) VALUES
(1, 2, 2, '2014-10-10 19:34:29', '2014-10-10 19:34:29'),
(3, 3, 1, '2014-10-13 09:17:54', '2014-10-14 13:16:10'),
(5, 3, 2, '2014-10-13 09:44:13', '2014-10-13 09:44:13'),
(6, 2, 1, '2014-10-13 09:45:21', '2014-10-21 17:25:06'),
(7, 4, 1, '2014-10-13 09:45:59', '2014-10-21 17:25:06'),
(8, 4, 2, '2014-10-13 09:46:22', '2014-10-13 09:46:22'),
(9, 5, 1, '2014-10-13 09:47:09', '2014-10-21 17:25:06'),
(10, 5, 2, '2014-10-13 09:47:34', '2014-10-13 09:47:34');

-- --------------------------------------------------------

--
-- Table structure for table `scheme`
--

CREATE TABLE IF NOT EXISTS `scheme` (
  `id` int(11) NOT NULL,
  `scheme_name` varchar(255) NOT NULL,
  `installment_amount` decimal(10,2) NOT NULL,
  `fine_start_day` int(2) NOT NULL,
  `installment_fine_amount` decimal(10,2) NOT NULL,
  `interest_fine_start_day` int(2) NOT NULL,
  `interest_fine_amount` decimal(10,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `scheme_name`, `installment_amount`, `fine_start_day`, `installment_fine_amount`, `interest_fine_start_day`, `interest_fine_amount`, `created_date`, `updated_date`) VALUES
(1, 'jay sardar', '1000.00', 11, '50.00', 11, '50.00', '2014-10-10 19:27:15', '2014-10-10 19:27:15'),
(2, 'gandhi', '1000.00', 11, '100.00', 11, '100.00', '2014-10-10 19:27:47', '2014-10-10 19:27:47'),
(3, 'Test Scheme', '1000.00', 11, '50.00', 11, '20.00', '2014-10-14 10:20:54', '2014-10-14 10:20:54');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_installment`
--

CREATE TABLE IF NOT EXISTS `scheme_installment` (
  `id` int(11) NOT NULL,
  `person_scheme_id` int(11) NOT NULL,
  `installment_amount` double NOT NULL,
  `fine_total` double NOT NULL,
  `total_amount` double NOT NULL,
  `is_paid` enum('YES','NO') NOT NULL,
  `due_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `comment` varchar(5000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_installment`
--

INSERT INTO `scheme_installment` (`id`, `person_scheme_id`, `installment_amount`, `fine_total`, `total_amount`, `is_paid`, `due_date`, `created_date`, `updated_date`, `comment`) VALUES
(2, 3, 1000, 1, 0, 'YES', '0000-00-00', '2014-12-24 17:07:59', '0000-00-00 00:00:00', ''),
(3, 7, 1000, 2, 0, 'YES', '0000-00-00', '2014-12-24 17:07:59', '0000-00-00 00:00:00', ''),
(4, 3, 1000, 0, 0, 'YES', '0000-00-00', '2014-12-24 17:14:57', '0000-00-00 00:00:00', ''),
(5, 6, 1000, 0, 0, 'YES', '0000-00-00', '2014-12-24 17:14:57', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `default_scheme_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `contact` varchar(20) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `default_scheme_id`, `name`, `email`, `username`, `password`, `gender`, `contact`, `role`, `created_date`, `updated_date`, `last_login`) VALUES
(1, 1, 'Priyank Khunt', 'priyankkhunt.freelance@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Male', '568932356', 'admin', '2014-10-05 00:00:00', '2014-10-05 00:00:00', '2014-10-05 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_credit`
--
ALTER TABLE `loan_credit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_interest`
--
ALTER TABLE `loan_interest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_scheme`
--
ALTER TABLE `person_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme`
--
ALTER TABLE `scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_installment`
--
ALTER TABLE `scheme_installment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `loan_credit`
--
ALTER TABLE `loan_credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `loan_interest`
--
ALTER TABLE `loan_interest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `person_scheme`
--
ALTER TABLE `person_scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `scheme`
--
ALTER TABLE `scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scheme_installment`
--
ALTER TABLE `scheme_installment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
