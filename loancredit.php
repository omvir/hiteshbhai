<?php
define("DEF", 1);
$page = 'loan';
require_once 'config.php';
include_once CLASSPATH.'class.person.php';
include_once CLASSPATH.'class.scheme.php';
include_once CLASSPATH.'class.loancredit.php';

$person = new Person();
$scheme = new Scheme();
$loanCredit = new LoanCredit();

if(isset($_POST['submit']))
{
    $loanCredit->addLoanCredit($_POST);
}

$person_res = $person->getPersons();
$scheme_res = $scheme->getSchemes();
$loanCredit_res = $loanCredit->getLoanCreditList();

include_once 'includes/header.php';
?>

<link href="<?php echo BASEURL; ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo BASEURL; ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?php echo BASEURL; ?>css/bootstrap-validator/bootstrap.validator.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo BASEURL; ?>js/plugins/bootstrap-validator/bootstrap.validator.min.js" type="text/javascript"></script>

<script src="<?php echo BASEURL; ?>js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
window.onload=function(){
    populatedropdown("dayLoanCreditDate", "monthLoanCreditDate", "yearLoanCreditDate", false);
}
    
$(document).ready(function(){
    
    $('#person_id').change(function(){
        $.ajax({
            type: 'post',
            data: {person_id : $(this).val()},
            url: './ajax/get_person_loan_schemes.php',
            success: function(data){
                $('#scheme_id').html(data);
            }
        });
    });
    
    $('#tbl_personLoanCreditList').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
    });
    
});

</script>

<!-- Here you can add extra css and js plugins -->
    </head>
    <body class="skin-blue">
        <?php include_once 'includes/top-block.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'includes/sidebar.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Loan Credit</h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Home </li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <?php include 'msg.php'; ?>
                                <form id="personLoanCreditForm" action="" method="post">
                                <div class="box-body clearfix">
                                    <div class="row">
                                        
                                        <div class="form-group col-md-3">
                                            <label>Person</label>
                                            <select class="form-control" id="person_id" name="person_id" required>
                                                <option value="">Select Person</option>
                                                <?php while($person_row = mysql_fetch_assoc($person_res)){ ?>

                                                <option value="<?php echo $person_row['id']; ?>"><?php echo $person_row['person_name']; ?></option>

                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label>Scheme</label>
                                            <select class="form-control" id="scheme_id" name="scheme_id" required>
                                                <option value="">Select Scheme</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>Date</label>
                                            <div class="clearfix">
                                                <div class="col-md-4"><select id="dayLoanCreditDate" name="dayLoanCreditDate" class="form-control"></select></div>
                                                <div class="col-md-4"><select id="monthLoanCreditDate" name="monthLoanCreditDate" class="form-control"></select></div>
                                                <div class="col-md-4"><select id="yearLoanCreditDate" name="yearLoanCreditDate" class="form-control"></select></div>
                                            </div>
                                        </div>

                                    </div>
                                    
                                    <div class="row">
                                            
                                        <div class="form-group col-md-3">
                                            <label>Credit Amount</label>
                                            <input type="text" class="form-control" id="credit_amount" name="credit_amount" placeholder="Credit Amount" required pattern="\d+(\.\d{2})?" />
                                        </div>


                                        <div class="form-group col-md-2">
                                            <label>&nbsp;</label>
                                            <button id="addPersonLoanCredit" type="submit" name="submit" class="form-control btn btn-primary" value="add">Loan Credit</button>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Loan Credit List</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body clearfix">
                                    <table id="tbl_personLoanCreditList" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Person Name</th>
                                                <th>Scheme Name</th>
                                                <th>Loan Amount</th>
                                                <th>Loan Interest</th>
                                                <th>Loan Date</th>
                                                <th>Credited Amount</th>
                                                <th>Last Updated Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            while($loanCredit_row = mysql_fetch_assoc($loanCredit_res))
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $loanCredit_row['person_name']; ?></td>
                                                <td><?php echo $loanCredit_row['scheme_name']; ?></td>
                                                <td><?php echo $loanCredit_row['total_amount']; ?></td>
                                                <td><?php echo $loanCredit_row['interest']; ?></td>
                                                <td><?php echo date('d F, Y',strtotime($loanCredit_row['loan_date'])); ?></td>
                                                <td><?php echo $loanCredit_row['loan_credit']; ?></td>
                                                <td><?php echo date('d-M-Y [h:i a]',strtotime($loanCredit_row['updated_date'])); ?></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->

    </body>
</html>