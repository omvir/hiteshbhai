<?php

defined("DEF") or die("Restricted Direct Entry...");

// Session Started Here ThereFore No To Start Anywhere Else
session_start();

// Define System Usable Paths and Urls
$base_url = "http://localhost/hiteshbhai/";
$base_path = dirname(__FILE__);

define("DS", DIRECTORY_SEPARATOR);
define("BASEURL",$base_url);
define("BASEPATH",$base_path.DS);
//define("ADMINURL",$base_url."admin/");
define("ADMINURL",$base_url);
//define("ADMINPATH",$base_path.DS."admin".DS);
define("ADMINPATH",$base_path.DS);
define("CLASSPATH",$base_path.DS."classes".DS);
define("LIBPATH",$base_path.DS."lib".DS);


// Define Database Detail Here.
$host = "localhost";
$user = "root";
$pass = "";
$dbname = "hiteshbhai";

define("HOST",$host);
define("USER",$user);
define("PASSWORD",$pass);
define("DB_NAME",$dbname);


// Autoloaded Classes and Libraries
require_once LIBPATH.'database.php';
require_once LIBPATH.'session.php';

$session = new SessionSet();