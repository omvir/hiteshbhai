<?php
define("DEF", 1);
require_once 'config.php';
include_once CLASSPATH.'class.scheme.php';

$page = 'dashboard';
$session->loginCheck("admin_logged_in",$page);

$scheme = new Scheme();

$scheme_res = $scheme->getSchemes();

include_once 'includes/header.php';
?>
<!-- Here you can add extra css and js plugins -->
</head>
<body class="skin-blue">
    <?php include_once 'includes/top-block.php'; ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <?php include_once 'includes/sidebar.php'; ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Dashboard</h1>
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i> Home </li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Hover Data Table</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="tbl_schemelist" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Scheme Name</th>
                                    <th>Installment Amount</th>
                                    <th>Started On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                while($scheme_row = mysql_fetch_assoc($scheme_res))
                                {
                                ?>
                                <tr>
                                    <td><?php echo $scheme_row['scheme_name']; ?></td>
                                    <td><?php echo $scheme_row['installment_amount']; ?></td>
                                    <td><?php echo date('d F,Y',strtotime($scheme_row['created_date'])); ?></td>
                                    <td></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </section>
        </aside>
    </div>

</body>
</html>
